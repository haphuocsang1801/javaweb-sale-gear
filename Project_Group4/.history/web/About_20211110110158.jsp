<%-- 
    Document   : About
    Created on : Nov 10, 2021, 10:40:27 AM
    Author     : sanghpce150201
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link
            href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;700&display=swap"
            rel="stylesheet"
            />
        <script
            src="https://kit.fontawesome.com/85150e2613.js"
            crossorigin="anonymous"
        ></script>
        <script
            type="module"
            src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"
        ></script>
        <script
            nomodule
            src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"
        ></script>
        <link
            href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
            rel="stylesheet"
            />
        <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="css/reset.css" rel="stylesheet" type="text/css" />
        <link href="css/style.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="wrapper">
            <section class="header">
                <jsp:include page="header.jsp" />
            </section>
            <section>
                <div class="about-us">
                    <div class="about-us__title">
                        <i class="fas fa-users"></i>About US
                    </div>
                    <table border="1">
                        <thead>
                            <tr>
                                <th>Avatar<th>
                                <th>Name</th>
                                <th>Student ID</th>
                                <th>Position</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td> <img class="" src="image/thanh.jpg" alt=""/></td>
                       
                                <td>Lâm Trung Thành</td>
                                <td>CE150868</td>
                                <td>Leader
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img class="" src="image/sang.jpg" alt=""/>
                                </td>
                                <td>Hà Phước Sang</td>
                                <td>CE150201</td>
                                <td>Developer
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img class="" src="image/toan.png" alt=""/>
                                    
                                </td>
                                <td>Nguyễn Văn Toàn</td>
                                <td>CE150811</td>
                                <td>Developer
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img class="" src="image/triet.jpg" alt=""/>
                                    
                                </td>
                                <td>Trương Minh Triết</td>
                                <td>CE150047</td>
                                <td>Developer
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img class="" src="image/liem.png" alt=""/>
                                    
                                </td>
                                <td>Trần Thanh Liêm</td>
                                <td>CE150783
                                </td>
                                <td>Developer
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </section>
            <section class="footer">
                <jsp:include page="footer.jsp" />
            </section>

    </body>
</html>
