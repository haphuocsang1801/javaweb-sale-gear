<%-- 
    Document   : InsertCategory
    Created on : Oct 25, 2021, 3:26:45 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="category-insert-input">
    <form action="MainController">        
        <div class="product-name">
            <div class="title">ID category</div>
            <div class="product-input">
              <input type="text" name="id_category" placeholder="id category" />
            </div>
          </div>
        <div class="product-name">
            <div class="title">Name</div>
            <div class="product-input">
              <input type="text" name="name_category" placeholder="name category" />
            </div>
          </div>
          <input type="submit" name="action" value="Insert" class="product-button">
    </form>
</div>
