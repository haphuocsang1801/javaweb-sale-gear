<%-- 
    Document   : InsertProduct
    Created on : Oct 25, 2021, 7:25:43 PM
    Author     : admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="MainControllerProduct">
            <div class="product-container">
                <input type="file" class="product-image">
                <div class="product-category">
                    <p>Category</p>
                    <select name="category">
                        <c:forEach begin="1" end="3" var="o" >
                        <option>LAPTOP</option>
                    </c:forEach>
                    </select>
                </div>
                <div class="product-name">
                    <p>Name of product</p>
                    <input type="text" name="name" id="">
                </div>
                <div class="product-quantity">
                    <p>Quantity</p>
                    <input type="number" name="" id="">
                </div>
            </div>
        </form>
    </body>
</html>
