<%-- 
    Document   : InsertProduct
    Created on : Oct 25, 2021, 7:25:43 PM
    Author     : admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="MainControllerProduct">
            <div class="product-container">
                <input type="file" name="image" class="product-image">
                <div class="product-category">
                    <p>Category</p>
                    <select name="category">
                        <c:forEach begin="1" end="3" var="o" >
                        <option>LAPTOP</option>
                    </c:forEach>
                    </select>
                </div>
                <div class="product-name">
                    <label for="name">Name of product</label>
                    <input type="text" name="name" id="">
                </div>
                <div class="product-quantity">
                    <label for="quantiry">Quantity</label>
                    <input type="number" name="quantity" id="">
                </div>
                <div class="product-price">
                    <label for="price">Price</label>
                    <input type="number" name="price" id="">
                </div>
                <div class="product-nxs">
                    <label for="nsx">Category</label>
                    <select name="nsx">
                        <c:forEach begin="1" end="3" var="o" >
                        <option>Dell</option>
                    </c:forEach>
                    </select>
                </div>
            </div>
        </form>
    </body>
</html>
