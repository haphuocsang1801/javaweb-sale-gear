<%-- Document : InsertProduct Created on : Oct 25, 2021, 7:25:43 PM Author :
admin --%> <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href="css/reset.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <title>JSP Page</title>
  </head>
  <body>
    <form action="MainControllerProduct">
      <div class="product-container">
        <div class="product-title">Add Product</div>
        <div class="product-name">
            <label for="name">Name of product</label>
            <input type="text" name="name" id="" />
          </div>
        
        <div class="product-category">
          <label for="category">Category</label>
          <select name="category">
            <c:forEach begin="1" end="3" var="o">
              <option>LAPTOP</option>
            </c:forEach>
          </select>
        </div>
        <div class="product-nxs">
            <label for="nsx">Producer</label>
            <select name="nsx">
              <c:forEach begin="1" end="3" var="o">
                <option>Dell</option>
              </c:forEach>
            </select>
          </div>
        
        <div class="product-quantity">
          <label for="quantiry">Quantity</label>
          <input type="number" name="quantity" id="" />
        </div>
        <div class="product-price">
          <label for="price">Price</label>
          <input type="number" name="price" id="" />
        </div>
        
        <div class="product-desc">
          <label for="desc">Description</label>
          <textarea name="desc" id="" cols="30" rows="10">
Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem maxime itaque molestias natus odio illum ab omnis optio vitae! Quasi laboriosam repudiandae veniam reprehenderit blanditiis enim sunt debitis id. Id?</textarea
          >
        </div>
      </div>
    </form>
  </body>
</html>
