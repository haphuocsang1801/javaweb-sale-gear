<%-- Document : InsertProduct Created on : Oct 25, 2021, 7:25:43 PM Author :
admin --%> <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href="css/reset.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <title>JSP Page</title>
  </head>
  <body>
    <form action="MainControllerProduct">
      <div class="product-container">
        <div class="product-title">Add Product</div>
        <div class="product-name">
            <div class="title" for="name">Name of product</div class="title">
            <input type="text" name="name" id="" />
          </div>
        
        <div class="product-category">
          <div class="title" for="category">Category</div class="title">
          <select name="category">
            <c:forEach begin="1" end="3" var="o">
              <option>LAPTOP</option>
            </c:forEach>
          </select>
        </div>
        <div class="product-nxs">
            <div class="title" for="nsx">Producer</div class="title">
            <select name="nsx">
              <c:forEach begin="1" end="3" var="o">
                <option>Dell</option>
              </c:forEach>
            </select>
          </div>
          <div class="product-image">
            <div class="title" for="image">Image</div class="title">
            <input type="file" name="image" />
          </div>
        <div class="product-quantity">
          <div class="title" for="quantiry">Quantity</div class="title">
          <input type="number" name="quantity" id="" />
        </div>
        <div class="product-price">
          <div class="title" for="price">Price</div class="title">
          <input type="number" name="price" id="" />
        </div>
        
        <div class="product-desc">
          <div class="title" for="desc">Description</div class="title">
          <textarea name="desc" id="" cols="30" rows="10"></textarea
          >
        </div>
      </div>
    </form>
  </body>
</html>
