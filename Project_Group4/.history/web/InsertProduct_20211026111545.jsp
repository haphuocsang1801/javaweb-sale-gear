<%-- Document : InsertProduct Created on : Oct 25, 2021, 7:25:43 PM Author :
admin --%> <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

      <form action="MainControllerProduct" enctype="multipart/form-data" method="post" class="product-form">
      <div class="product-container">
        <div class="product-title">Add Product</div>
        <div class="product-name">
          <div class="title">Product Code</div>
          <div class="product-input">
            <input type="text" name="id" id="" placeholder="Product ID"/>
          </div>
        </div>
        <div class="product-name">
          <div class="title">Product name</div>
          <div class="product-input">
            <input type="text" name="name" id="" placeholder="Product name"/>
          </div>
        </div>
        <div class="product-category">
          <div class="title">Category</div>
          <select name="category">
              <c:forEach items="${requestScope.LIST_CATEGORY}" var="o">
                  <option value="${o.id}">${o.name}</option>
            </c:forEach>
          </select>
        </div>
        <div class="product-nxs">
          <div class="title">Producer</div>
          <select name="nsx">
              <c:forEach items="${requestScope.LIST_Producers}" var="o">
                  <option value="${o.nsx_id}" >${o.nsx_name}</option>
            </c:forEach>
          </select>
        </div>
        <div class="product-image">
          <div class="title">Image</div>
          <input type="file" name="image" />
        </div>
        <div class="product-quantity">
          <div class="title">Quantity</div>
          <input type="number" name="quantity" id="" />
        </div>
        <div class="product-price">
          <div class="title">Price</div>
          <input type="number" name="price" id="" />
        </div>

        <div class="product-desc">
          <div class="title">Description</div>
          <textarea name="desc" id="" cols="30" rows="10"></textarea>
        </div>
        <div class="product-submit">
          <input type="submit" name="action" value="Add product">
          <input type="submit" name="action" value="Back">
        </div>
      </div>
      
    </form>
