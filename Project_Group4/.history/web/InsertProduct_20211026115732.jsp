<%-- Document : InsertProduct Created on : Oct 25, 2021, 7:25:43 PM Author :
admin --%> <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link
      href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;700&display=swap"
      rel="stylesheet"
    />
    <script
      src="https://kit.fontawesome.com/85150e2613.js"
      crossorigin="anonymous"
    ></script>
    <script
      type="module"
      src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"
    ></script>
    <script
      nomodule
      src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"
    ></script>
    <link href="css/reset.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <title>JSP Page</title>
  </head>
  <body>
    <form
      action="MainControllerProduct"
      enctype="multipart/form-data"
      method="post"
      class="product-form"
    >
      <div class="product-container">
        <div class="product-title">Add Product</div>
        <div class="product-name">
          <div class="title">Product Code</div>
          <div class="product-input">
            <input type="text" name="id" id="" placeholder="Product ID" />
          </div>
        </div>
        <div class="product-name">
          <div class="title">Product name</div>
          <div class="product-input">
            <input type="text" name="name" id="" placeholder="Product name" />
          </div>
        </div>
       <div class="product-select">
        <div class="product-select-items product-category">
          <div class="title">Category</div>
          <div class="product-input">
            <select name="category">
              <c:forEach items="${requestScope.LIST_CATEGORY}" var="o">
                <option value="${o.id}">${o.name}</option>
              </c:forEach>
            </select>
          </div>
        </div>
        <div class="product-select-items product-nxs">
          <div class="title">Producer</div>
          <div class="product-input">
            <select name="nsx">
              <c:forEach items="${requestScope.LIST_Producers}" var="o">
                <option value="${o.nsx_id}">${o.nsx_name}</option>
              </c:forEach>
            </select>
          </div>
        </div>
       </div>
        <div class="product-image">
          <div class="title">Image</div>
          <input type="file" name="image" />
        </div>
        <div class="product-name">
          <div class="title">Quantity</div>
          <div class="product-input">
            <input type="number" name="quantity" id="" placeholder="Product ID" />
          </div>
        </div>
        <div class="product-name">
          <div class="title">Price</div>
          <div class="product-input">
            <input type="text" name="price" id="" placeholder="Product ID" />
          </div>
        </div>
        <div class="product-desc">
          <div class="title">Description</div>
          <textarea style="resizse" name="desc" id="" cols="30" rows="10"></textarea>
        </div>
        <div class="product-submit">
          <input type="submit" name="action" value="Add product" />
          <input type="submit" name="action" value="Back" />
        </div>
      </div>
    </form>
  </body>
</html>
