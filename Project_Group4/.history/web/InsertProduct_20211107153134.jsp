<%-- Document : InsertProduct Created on : Oct 25, 2021, 7:25:43 PM Author :
admin --%> <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
        <form
            action="MainControllerProduct"
            enctype="multipart/form-data"
            method="post"
            class="product-form"
            >
            <div class="product-container">
                <div class="product-title">Add Product</div>
                <div class="product-name">
                    <div class="title">Product Code</div>
                    <div class="product-input">
                        <input type="text" name="id" id="" placeholder="Product ID" required=""/>
                    </div>
                </div>
                <div class="product-name">
                    <div class="title">Product name</div>
                    <div class="product-input">
                        <input type="text" name="name" id="" placeholder="Product name" required=""/>
                    </div>
                </div>
                <div class="product-select">
                    <div class="product-select-items product-category">
                        <div class="title">Category</div>
                        <div class="product-input">
                            <select name="category">
                                <c:forEach items="${requestScope.LIST_CATEGORY}" var="o">
                                    <option value="${o.id}">${o.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="product-select-items product-nxs">
                        <div class="title">Producer</div>
                        <div class="product-input">
                            <select name="nsx">
                                <c:forEach items="${requestScope.LIST_Producers}" var="o">
                                    <option value="${o.nsx_id}">${o.nsx_name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="product-image">
                    <div class="title">Image</div>
                    <input type="file" name="image" required=""/>
                </div>
                <div class="product-name">
                    <div class="title">Quantity</div>
                    <div class="product-input">
                        <input type="number" name="quantity" id="" placeholder="Product quantity" required=""/>
                    </div>
                </div>
                <div class="product-name">
                    <div class="title">Price</div>
                    <div class="product-input">
                        <input type="text" name="price" id="" placeholder="Product price" required="" />
                    </div>
                </div>
                <div class="product-name">
                    <div class="title">Description</div>
                    <div class="product-input product-textarea">
                        <textarea style="resize: none;" name="desc" placeholder=""></textarea>
                    </div>
                </div>
                <div class="product-submit">
                    <input type="submit" name="action" value="Add product" class="product-button"/>
                    <input type="submit" name="action" value="Back" class="product-button product-back"/>
                </div>
            </div>
        </form>
