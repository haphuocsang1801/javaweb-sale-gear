<%-- Document : ListProduct Created on : Oct 2, 2021, 11:59:25 PM Author : admin
--%> <%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>JSP Page</title>
    <link href="css/reset.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <div class="wrapper">
      <section class="header">
        <jsp:include page="header.html" />
      </section>
      <section class="product-list">
        <jsp:include page="product.html" />
      </section>
    </div>
    <section class="footer">
      <jsp:include page="footer.html" />
    </section>
  </body>
</html>
