<%-- 
    Document   : admin
    Created on : Oct 21, 2021, 9:07:21 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin Page</title>
    </head>
    <body>
        <a href="index.jsp" class="header-logo">
            <img src="./image/Logo.png" alt="" class="header-logo-image" />
        </a>
        <ul class="header-menu">
            <li class="header-menu-item">
                <a href="index.jsp" class="header-menu-link">Home</a>
            </li>
            <li class="header-menu-item hover">
                <div class="header-menu-product">
                    <a href="ListProduct.jsp" class="header-menu-link">Product </a>
                    <ion-icon name="chevron-down-outline" class="menu-down"></ion-icon>
                </div>
                <div class="header-menu-child">
                    <div class="menu-child-item">
                        <a href="#"
                           ><ion-icon name="laptop-outline"></ion-icon>
                            Laptop
                        </a>
                    </div>
                    <div class="menu-child-item">
                        <a href="#"
                           ><i class="fas fa-desktop"></i>
                            PC
                        </a>
                    </div>
                    <div class="menu-child-item">
                        <a href="#"><i class="fas fa-microchip"></i> PC Components</a>
                    </div>
                    <div class="menu-child-item">
                        <a href="#"
                           ><i class="far fa-keyboard"></i>
                            Keyboard
                        </a>
                    </div>
                    <div class="menu-child-item">
                        <a href="#"
                           ><i class="fas fa-mouse"></i>
                            Mouse
                        </a>
                    </div>
                    <div class="menu-child-item">
                        <a href="#"
                           ><i class="fas fa-headset"></i>
                            Head Phone
                        </a>
                    </div>
                    <div class="menu-child-item">
                        <a href="#"
                           ><i class="fas fa-chair"></i>
                            Chair
                        </a>
                    </div>
                    <div class="menu-child-item">
                        <a href="#"
                           ><i class="fas fa-mouse"></i>
                            Mouse
                        </a>
                    </div>
                </div>
            </li>  
                    <li class="header-menu-item">
                        <a href="#" class="header-menu-link">Admin</a>
                    </li>
        </ul>       
      
        <div class="header-auth">
            <a href="login.jsp" class="button button--header">Login</a>
            <a href="signup.jsp" class="button button--primary">Sign up</a>
        </div>
        <%
            } else if (loginUser != null) {
                %>
                 <div class="header-user">
          <a href="#" class="header-user-image">
            <img src="./image/userr.jpg" alt="" />
            <div class="header-user-info">
              <h3 class="header-user-name"><%=loginUser.getFullname()%></h3>
            </div>
          </a>
          <a class="header-user-icon" href="logout" >
              <i class="fas fa-sign-out-alt">    
              </i>
          </a>
        </div>      
        <%
            }
        %>
        
        
        
    </body>
</html>
