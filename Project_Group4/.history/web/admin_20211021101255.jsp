<%-- Document : admin Created on : Oct 21, 2021, 9:07:21 AM Author : admin --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Admin Page</title>
  </head>
  <body>
    <a href="index.jsp" class="header-logo">
      <img src="./image/Logo.png" alt="" class="header-logo-image" />
    </a>
    <ul class="header-menu">
      <li class="header-menu-item">
        <a href="index.jsp" class="header-menu-link">Product</a>
      </li>
      <li class="header-menu-item">
        <a href="index.jsp" class="header-menu-link">Account</a>
      </li>

    </ul>
    <div class="header-user">
      <a href="#" class="header-user-image">
        <img src="./image/userr.jpg" alt="" />
        <div class="header-user-info">
          <h3 class="header-user-name">Admin</h3>
        </div>
      </a>
      <a class="header-user-icon" href="logout">
        <i class="fas fa-sign-out-alt"> </i>
      </a>
    </div>
  </body>
</html>
