<%-- Document : admin Created on : Oct 21, 2021, 9:07:21 AM Author : admin --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Admin Page</title>
    <link href="css/reset.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <div class="modify-left">
      <h2>Danh muc</h2>
      <ul>
        <li>
          <a href="Category">Loai san pham</a>
        </li>
        <li>
          <a href="product">San pham</a>
        </li>
      </ul>
    </div>
    <div class="list-product">
      <div class="table-category">
        <form action="MainController">
          <table border="1">
            <thead>
              <tr>
                <th>STT</th>
                <th>Name</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Laptop</td>
              </tr>
            </tbody>
          </table>
        </form>
      </div>
    </div>
  </body>
</html>
