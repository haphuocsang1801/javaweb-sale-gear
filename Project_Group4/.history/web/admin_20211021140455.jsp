<%-- Document : admin Created on : Oct 21, 2021, 9:07:21 AM Author : admin --%>
<%@page import="entity.User"%>
<%@page import="java.util.List"%>
<%@page import="entity.Category"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Admin Page</title>
        <link href="css/reset.css" rel="stylesheet" type="text/css" />
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <style>
            table,
            th,
            td {
                border: 1px solid black;
            }
        </style>
    </head>
    <body>
        <%
            User loginUser = (User) session.getAttribute("LOGIN_USER");
            if(loginUser==null){
                response.sendRedirect("index.jsp");
            }
            
        %>
        <div class="container-admin">
            <div class="modify-left">
                <h2>Danh muc</h2>
                <ul>
                    <li>
                        <a href="MainController?action=Show_category">Loai san pham</a>
                    </li>
                    <li>
                        <a href="product">San pham</a>
                    </li>
                </ul>
            </div>
            <%
                List<Category> list = (List<Category>) request.getAttribute("SHOW_CATEGORY");
                if (list != null) {
                    if (!list.isEmpty()) {
            %>
            <div class="list-product">
                <div class="table-category">         
                    <form action="MainController">
                        <table>
                        <tr>
                            <th>STT</th>
                            <th>Name</th>
                            <th>Update</th>
                            <th>Delete</th>  
                        </tr>
                        <%
                            for (Category elem : list) {
                        %>
                        <tr>
                            <td><%=elem.getId()%></td>
                            <td>
                                <input  type="text" name="name" value="<%=elem.getName()%>"/>
                                <input  type="hidden" name="id" value="<%=elem.getId()%>"/>
                            </td>
                       
                              <td><input type="submit" name="action" value="Update-Category"/></td>
                              <td><input type="submit" name="action" value="Delete-Category"/></td>                   
                        </tr>  

                        <%
                            }
                        %>

                    </table>
                    </form>
                </div>
            </div>
            <%
                    }
                }
            %>
            .
        </div>
    </body>
</html>
