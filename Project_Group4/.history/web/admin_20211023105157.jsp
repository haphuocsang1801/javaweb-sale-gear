<%-- Document : admin Created on : Oct 21, 2021, 9:07:21 AM Author : admin --%>
<%@page import="entity.User"%>
<%@page import="java.util.List"%>
<%@page import="entity.Category"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Admin Page</title>
        <link href="css/reset.css" rel="stylesheet" type="text/css" />
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <style>
            table,
            th,
            td {
                border: 1px solid black;
            }
        </style>
    </head>
    <body>      
        <%
            User loginUser = (User) session.getAttribute("LOGIN_USER");
            if (loginUser == null) {
                response.sendRedirect("index.jsp");
            }

        %>
        <div class="container-admin">
            <div class="category">
                <h1>Danh muc</h1>
                <ul>
                    <li>
                        <a href="MainController?action=search_category">Manage Category of Product</a>
                    </li>
                    <li>
                        <a href="MainController?action=Show_product">Manage Product</a>
                    </li>
                </ul>
            </div>
            <% 
                String check = (String) session.getAttribute("INPUT_INSERT");            
                List<Category> list=(List<Category>) request.getAttribute("LIST_CATEGORY");
                 String search= (String) request.getAttribute("search");
                if(search==null){
                    search="";
                }
                if (list != null) {
                    if (!list.isEmpty()) {
            %>
            
            <div class="list-product">
                <div class="insert_catetgory">
                    <a href="ShowInputController?action=Show_insert">Insert</a>  
                </div>      
                <form action="MainController">
                    Search<input type="text" name="search" value="<%=search%>"/>
                    <input type="submit" value="search_category" name="action"/>
                </form>
                <div class="table-category">         
                    <table>
                        <tr>
                            <th>STT</th>
                            <th>Name</th>
                            <th>Update</th>
                            <th>Delete</th>  
                        </tr>
                        <%
                            for (Category elem : list) {
                        %>
                        <form action="MainController">
                            <tr>
                                <td>
                                    <%=elem.getId()%>
                                    <input type="hidden" name="id" value="<%=elem.getId()%>"/>
                                </td>
                                <td>
                                    <input  type="text" name="name" value="<%=elem.getName()%>"/>

                                </td>                       
                                <td><input type="submit" name="action" value="Update-Category"/></td>

                                <td><input type="submit" name="action" value="Delete-Category"/></td>                               
                            </tr>  
                            <%String error = (String) request.getAttribute("ERROR");
                                if (error == null) {
                                    error = null;
                                } else {
                            %>
                            <h1>
                                <%=error%>
                            </h1> 
                            <%
                                }
                            %>
                        </form>  

                        <%
                            }
                        %>
                    </table>
                </div>    
            </div>
            <%
                }
            } else if (list == null && check == null) {
            %>
            <h1>Please select function</h1>
            <%
                }
            %>
            <%
                if (check == null) {
                    return;
                } else if (check.equals("Show_insert")) {
            %>
            <form action="MainController">
                <input type="number" name="id_category"/><br>
                <input type="text" name="name_category"/><br>
                <input type="submit" name="action" value="Insert">
            </form>
            <%
                    session.setAttribute("INPUT_INSERT", null);
                }
            %>
        </div>
    </body>
</html>
