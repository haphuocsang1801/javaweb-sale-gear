<%-- Document : admin Created on : Oct 21, 2021, 9:07:21 AM Author : admin --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="entity.User"%>
<%@page import="java.util.List"%>
<%@page import="entity.Category"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Admin Page</title>
        <link
            href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;700&display=swap"
            rel="stylesheet"
            />
        <script
            src="https://kit.fontawesome.com/85150e2613.js"
            crossorigin="anonymous"
        ></script>
        <script
            type="module"
            src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"
        ></script>
        <script
            nomodule
            src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"
        ></script>

        <link href="css/reset.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <style>
            table,
            th,
            td {
                border: 1px solid black;
            }
        </style>
    </head>
    <body>
        <div class="wrapper">
            <section class="header">
                <jsp:include page="header.jsp"/> 
            </section>
            <section class="admin">
                <%
                    User loginUser = (User) session.getAttribute("LOGIN_USER");
                    if (loginUser == null) {
                        response.sendRedirect("index.jsp");
                    }

                %>
                <div class="container-admin">
                    <div class="category-manage">
                        <h1>Management</h1>
                        <ul>
                            <li>
                                <i class="fas fa-caret-right"></i>
                                <a href="MainController?action=search_category">Manage Category</a>
                            </li>
                            <li>
                                <i class="fas fa-caret-right"></i>
                                <a href="MainController?action=Show_product">Manage Product</a>
                            </li>
                        </ul>
                    </div>
                    <%                        
                        String check = (String) session.getAttribute("INPUT_INSERT");
                        List<Category> list = (List<Category>) request.getAttribute("LIST_CATEGORY");
                        String search = (String) request.getAttribute("search");
                        if (search == null) {
                            search = "";
                        }
                        if (list != null) {
                            if (!list.isEmpty()) {
                    %>

                    <div class="category-list">
                        <div class="category-top">
                            <div class="category-insert">
                                <a href="ShowInputController?action=Show_insert">Insert</a>  
                            </div>      
                            <div class="category-search">
                                <form action="MainController">
                                    <input type="text" name="search" classvalue="<%=search%>"/>
                                    <input type="submit" value="search_category" name="action"/>
                                </form>           
                            </div>
                        </div>
                        <div class="category-bottom">         
                            <table>
                                <tr>
                                    <th>STT</th>
                                    <th>Name</th>
                                    <th>Update</th>
                                    <th>Delete</th>  
                                </tr>
                                <%
                                    for (Category elem : list) {
                                %>
                                <form action="MainController">
                                    <tr>
                                        <td>
                                            <%=elem.getId()%>
                                            <input type="hidden" name="id" value="<%=elem.getId()%>"/>
                                        </td>
                                        <td>
                                            <input  type="text" name="name" value="<%=elem.getName()%>"/>

                                        </td>                       
                                        <td><input type="submit" name="action" value="Update-Category"/></td>

                                        <td><input type="submit" name="action" value="Delete-Category"/></td>                               
                                    </tr>  
                                    <%String error = (String) request.getAttribute("ERROR");
                                        if (error == null) {
                                            error = null;
                                        } else {
                                    %>
                                    <h1>
                                        <%=error%>
                                    </h1> 
                                    <%
                                        }
                                    %>
                                </form>
                                <%
                                    }
                                %>
                            </table>
                        </div>    
                    </div>
                    <%
                        }
                    } else if (list == null && check == null) {
                    %>
                    <h1>Please select function</h1>
                    <%
                        }
                    %>
                    <%
                        if (check == null) {
                            check=null;
                        } else if (check.equals("Show_insert")) {
                    %>
                    <div class="category-insert-input">
                        <form action="MainController">
                            <input type="number" name="id_category"/><br>
                            <input type="text" name="name_category"/><br>
                            <input type="submit" name="action" value="Insert">
                        </form>
                    </div>
                    <%
                            session.setAttribute("INPUT_INSERT", null);
                        }
                    %>
                </div>
            </section>        
        </div>
        <section class="footer">
            <jsp:include page="footer.jsp"/> 
        </section>
    </body>
</html>
