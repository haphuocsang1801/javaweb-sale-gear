<%-- Document : admin Created on : Oct 21, 2021, 9:07:21 AM Author : admin --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="entity.User"%>
<%@page import="java.util.List"%>
<%@page import="entity.Category"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Admin Page</title>
        <link
            href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;700&display=swap"
            rel="stylesheet"
            />
        <script
            src="https://kit.fontawesome.com/85150e2613.js"
            crossorigin="anonymous"
        ></script>
        <script
            type="module"
            src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"
        ></script>
        <script
            nomodule
            src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"
        ></script>

        <link href="css/reset.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <style>
            table,
            th,
            td {
                border: 1px solid #cccc;        
            }
        </style>
    </head>
    <body>
        <div class="wrapper">
            <section class="header">
                <jsp:include page="header.jsp"/> 
            </section>
            <section class="admin">
                <%
                    User loginUser = (User) session.getAttribute("LOGIN_USER");
                    if (loginUser == null) {
                        response.sendRedirect("index.jsp");
                    }
                    String display_manage = (String) request.getAttribute("Show_manage");
                %>
                <div class="container-admin">
                    <div class="admin-manage">
                      
                        <ul>
                            <li>
                                <i class="fas fa-caret-right"></i>
                                <a href="MainController?action=search_category">Manage Category</a>
                            </li>
                            <li>
                                <i class="fas fa-caret-right"></i>
                                <a href="MainControllerProduct?action=search">Manage Product</a>
                            </li>
                        </ul>
                    </div>
                    <%
                        if (display_manage == "category") {
                    %>
                    <jsp:include page="categoryAdmin.jsp"/>
                    <%
                    } else if (display_manage == "product") {
                    %>
                    <jsp:include page="productAdmin.jsp"/>
                    <%}%>
                </div>
            </section>        
        </div>
        <section class="footer">
            <jsp:include page="footer.jsp"/> 
        </section>
    </body>
</html>
