<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="container px-3 my-5 clearfix">
  <!-- Shopping cart table -->
  <c:set var="o" value="${sessionScope.CART}" />
  <div class="card">
      <c:if test="${empty o}">
          <a href=""><i class="fas fa-cart-arrow-down"></i></a>
          <h1>There are no products in your cart yet.</h1>
      </c:if>
    <div class="card-header">
      <h2>Shopping Cart</h2>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered m-0">
          <thead>
            <tr>
              <!-- Set columns width -->
              <th class="text-center py-3 px-4" style="min-width: 400px">
                Product Name &amp; Details
              </th>
              <th class="text-right py-3 px-4" style="width: 100px">Price</th>
              <th class="text-center py-3 px-4" style="width: 120px">
                Quantity
              </th>
              <th class="text-right py-3 px-4" style="width: 100px">Total</th>
              <th
                class="text-center align-middle py-3 px-0"
                style="width: 40px"
              >
              <i class="fas fa-trash"></i>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="p-4">
                <div class="media align-items-center">
                  <img
                    src="https://bootdey.com/img/Content/avatar/avatar1.png"
                    class="d-block ui-w-40 ui-bordered mr-4"
                    alt=""
                  />
                  <div class="media-body">
                    <a href="#" class="d-block text-dark">Product 1</a>
                    <small>
                      <span class="text-muted">Laptop Lenovo IdeaPad 5 Pro 16ACH6 82L50095VN</span>
                     
                    </small>
                  </div>
                </div>
              </td>
              <td class="text-right font-weight-semibold align-middle p-4">
                $57.55
              </td>
              <td class="align-middle p-4">
                <input type="text" class="form-control text-center" value="2" />
              </td>
              <td class="text-right font-weight-semibold align-middle p-4">
                $115.1
              </td>
              <td class="text-center align-middle px-0">
                <i style="cursor: pointer;" class="fas fa-times"></i>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <!-- / Shopping cart table -->

      <div
        class="d-flex flex-wrap justify-content-between align-items-center pb-4"
      >
        <div class="d-flex">
          <div class="text-right mt-4">
            <label class="text-muted font-weight-normal m-0">Total price</label>
            <div class="text-large"><strong>$1164.65</strong></div>
          </div>
        </div>
      </div>

      <div class="float-right">
        <button
          type="button"
          class="btn btn-lg btn-default md-btn-flat mt-2 mr-3"
        >
          Back to shopping
        </button>
        <button type="button" class="button button--primary">Checkout</button>
      </div>
    </div>
  </div>
</div>
