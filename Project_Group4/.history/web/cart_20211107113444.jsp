<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="container px-3 my-5 clearfix">
    <!-- Shopping cart table -->
    <c:set var="o" value="${sessionScope.CART.cart}" />
    <h1 class="cart-title">${requestScope.CHECKOUT} </h1>
    <c:if test="${not empty requestScope.CHECKOUT}">
       
    </c:if>
    <c:if test="${empty requestScope.CHECKOUT}">
        <c:if test="${empty o}">
            <div class="cart-back">
                <a href="product" class="cart-icon"><i class="fas fa-cart-arrow-down"></i></a>
                <h1 class="cart-title">No products in the cart</h1>
            </div>
        </c:if>
        <c:if test="${not empty o}">
            <c:set var="total" value="${0}"/>
            <div class="card">
                <div class="card-header">
                    <h2>Shopping Cart</h2>
                </div>
                <form action="CartController">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered m-0">
                                <thead>
                                    <tr>
                                        <!-- Set columns width -->
                                        <th class="text-center py-3 px-3" style="min-width: 400px">
                                            Product Name &amp; Details
                                        </th>
                                        <th class="text-right py-3 px-3" style="width: 100px">Price</th>
                                        <th class="text-center py-3 px-3" style="width: 120px">
                                            Quantity
                                        </th>
                                        <th class="text-right py-3 px-3" style="width: 100px">Total</th>
                                        <th class="text-right py-3 px-3" style="width: 40px">
                                            Update
                                        </th>
                                        <th
                                            class="text-center align-middle py-3 px-0"
                                            style="width: 40px"
                                            >
                                            <i class="fas fa-trash"></i>
                                        </th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="entry" items="${sessionScope.CART.cart}">
                                        <c:set var="total" value="${total + entry.value.quanity_cart*entry.value.product.price}" />
                                    <form action="CartController">
                                        <tr>
                                            <td class="p-3">
                                                <div class="media align-items-center">
                                                    <img
                                                        src="${entry.value.product.image}"
                                                        class="d-block ui-w-40 ui-bordered mr-4"
                                                        alt=""
                                                        />
                                                    <div class="media-body">
                                                        <small>
                                                            <span class="text-muted">${entry.value.product.name}</span>
                                                        </small>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-right font-weight-semibold align-middle p-3">
                                                $${entry.value.product.price}
                                            </td>
                                            <td class="align-middle p-4">
                                                <input type="number" class="form-control text-center" name="cart-quantity" value="${entry.value.quanity_cart}" />
                                            </td>
                                            <td class="text-right font-weight-semibold align-middle p-3">
                                                $${entry.value.quanity_cart*entry.value.product.price}
                                            </td>
                                        <input type="hidden" name="pro-id" value="${entry.value.product.id}"/>
                                        <td class="text-center align-middle px-0">
                                            <button
                                                type="submit"
                                                class="text-center align-middle px-0" name="action" value="update" ><i style="cursor: pointer;" class="fas fa-sync-alt"></i>
                                            </button>    
                                        </td>
                                        <td class="text-center align-middle px-0">
                                            <button
                                                type="submit"
                                                class="text-center align-middle px-0" name="action" value="delete" ><i style="cursor: pointer;" class="fas fa-times"></i>
                                            </button>
                                        </td>
                                        </tr>
                                    </form>     
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                        <!-- / Shopping cart table -->
                        <div
                            class="d-flex flex-wrap justify-content-between align-items-center pb-4"
                            >
                            <div class="d-flex">
                                <div class="text-right mt-4">
                                    <label class="text-muted font-weight-normal m-0">Total price</label>
                                    <div class="text-large"><strong>$${total}</strong></div>
                                     <p class="dangerous"> ${requestScope.Out_of_stock}</p>
                                </div>
                            </div>
                        </div>
                        <div class="float-right">
                            <button
                                type="submit"
                                class="btn btn-lg btn-default md-btn-flat mt-2 mr-3" name="action" value="back-buy">Back to shopping
                            </button>
                            <button type="submit" class="button button--primary" name="action" value="checkout">
                                Checkout
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </c:if>
    </c:if>
</div>
