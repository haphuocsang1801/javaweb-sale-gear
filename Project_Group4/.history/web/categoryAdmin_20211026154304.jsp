<%-- 
    Document   : categoryAdmin
    Created on : Oct 24, 2021, 9:36:53 AM
    Author     : admin
--%>

<%@page import="java.util.List"%>
<%@page import="entity.Category"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%                                        
                        List<Category> list_categorys = (List<Category>) request.getAttribute("LIST_CATEGORY");//list category
                        String search = (String) request.getAttribute("search");
                        if (search == null) {
                            search = "";
                        }
                        if (list_categorys != null) {
                            if (!list_categorys.isEmpty()) {
                    %>

                    <div class="category-list">
                        <div class="category-top">
                            <div class="category-insert">
                                <a href="ShowInputController?action=ShowinsertCategory"><i class="fas fa-plus"></i>Insert</a>  
                            </div>      
                            <div class="category-action">
                                <form action="MainController">
                                    <input type="text" name="search" class="product-input" 
                                   placeholder="name" value="<%=search%>"/>
                                    <input type="submit" value="Search" name="action" class="product-button"/>
                                </form>           
                            </div>
                        </div>
                        <div class="category-bottom" >         
                            <table>
                                <tr>
                                    <th>STT</th>
                                    <th>Name</th>
                                    <th>Update</th>
                                    <th>Delete</th>  
                                </tr>
                                <%
                                    for (Category elem : list_categorys) {
                                %>
                                <form action="MainController">
                                    <tr>
                                        <td>
                                            <%=elem.getId()%>
                                            <input type="hidden" name="id" value="<%=elem.getId()%>"/>
                                        </td>
                                        <td>
                                            <input  type="text" name="name" value="<%=elem.getName()%>"/>

                                        </td>                       
                                        <td><input type="submit" name="action" value="Update-Category"/></td>

                                        <td><input type="submit" name="action" value="Delete-Category"/></td>                               
                                    </tr>  
                                    <%String error = (String) request.getAttribute("ERROR");
                                        if (error == null) {
                                            error = null;
                                        } else {
                                    %>
                                    <h1>
                                        <%=error%>
                                    </h1> 
                                    <%
                                        }
                                    %>
                                </form>
                                <%
                                    }
                                %>
                            </table>
                        </div>    
                    </div>
                    <%
                        }
                    }
                    %>
                             
                   
