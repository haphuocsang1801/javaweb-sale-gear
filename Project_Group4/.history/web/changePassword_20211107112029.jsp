<%-- Document : changePassword Created on : Nov 6, 2021, 8:16:49 PM Author :
sanghpce150201 --%> <%@ taglib prefix="c"
uri="http://java.sun.com/jsp/jstl/core" %> <%@page contentType="text/html"
pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>JSP Page</title>
    <link
      href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;700&display=swap"
      rel="stylesheet"
    />
    <script
      src="https://kit.fontawesome.com/85150e2613.js"
      crossorigin="anonymous"
    ></script>
    <script
      type="module"
      src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"
    ></script>
    <script
      nomodule
      src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"
    ></script>
    <link
      href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
      rel="stylesheet"
    />
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="css/reset.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <div class="wrapper">
      <section class="header">
        <jsp:include page="header.jsp" />
      </section>
      <section class="login">
        <form action="MainController" method="get" autocomplete="off">
          <div class="login-main">
            <div class="login-title">Change Password</div>
            <div class="login-field">
              <i class="fa fa-lock login-icon"> </i>
              <input
                type="password"
                class="login-input"
                name="current"
                placeholder="Enter your current password"
              />
            </div>
            <c:if test="${not empty requestScope.error.current_password}">
              <div class="dangerous">
                <i class="fas fa-exclamation"></i
                >${requestScope.error.current_password}
              </div>
            </c:if>
            <div class="login-field">
              <i class="fa fa-lock login-icon"> </i>
              <input
                type="password"
                class="login-input"
                placeholder="New password"
                name="password"
              />
            </div>
            <c:if
              test="${(empty requestScope.error.current_password) and (not empty requestScope.error.new_password)}"
            >
              <div class="dangerous">
                <i class="fas fa-exclamation"></i
                >${requestScope.error.new_password}
              </div>
            </c:if>
            <div class="login-field">
              <i class="fa fa-lock login-icon"> </i>
              <input
                type="password"
                class="login-input"
                placeholder="Re-type new password"
                name="re-password"
              />
            </div>
            <c:if
              test="${(not empty requestScope.userError) and (empty requestScope.error.new_password) and (empty requestScope.error.current_password)}"
            >
              <div class="dangerous">
                <i class="fas fa-exclamation"></i
                >${requestScope.userError.passwordError}
              </div>
            </c:if>
            <button class="login-submit" value="changePassword" name="action">
              Save changes
            </button>

            <c:if test="${not empty requestScope.CHANGE_SUCCESS}">
              <div class="dangerous-success">
                <ion-icon name="checkmark-sharp"></ion-icon>
                ${requestScope.CHANGE_SUCCESS}
              </div>
            </c:if>
          </div>
        </form>
      </section>
    </div>
    <footer class="footer footer--login">
      <jsp:include page="footer.jsp" />
    </footer>
  </body>
</html>
