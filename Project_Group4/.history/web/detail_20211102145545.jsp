
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="container bootdey">
      <c:set var="o" value="${requestScope.PRODUCT_DETAIL}" />
    <div class="col-md-12">
        <section class="panel">
            <div class="panel-body">
                <div class="col-md-6">
                    <div class="pro-img-details">
                        <img
                            src="${o.image}"
                            alt=""
                            />
                    </div>
                </div>
                <div class="col-md-6">
                    <h4 class="pro-d-title">
                       ${o.name}</a>
                    </h4>
                    <p class="footer-desc">
                        ${o.desc}
                    </p>
                    <div class="product_meta">
                        <span class="tagged_as"
                              ><strong>Producer:</strong> <a rel="tag" href="#">${o.nsx_name}</a>
                    </div>
                    <div class="m-bot15">
                        <strong>Price : </strong>
                        <span class="pro-price"> $${o.price}</span>
                    </div>
                    <div class="form-group">
                        <label>Quantity</label>
                        <input
                            type="number" min="1"
                            placeholder="1"
                            class="form-control quantity"
                            />
                    </div>
                    <p>
                        <button class="button button--primary" style="border: none;
                                " type="button">
                            <i class="fa fa-shopping-cart"></i> Add to Cart
                        </button>
                    </p>
                </div>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript"></script>
