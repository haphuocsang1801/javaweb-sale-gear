
        <a href="index.jsp" class="header-logo">
          <img src="./image/Logo.png" alt="" class="header-logo-image" />
        </a>
        <ul class="header-menu">
          <li class="header-menu-item">
            <a href="#" class="header-menu-link">Home</a>
          </li>
          <li class="header-menu-item hover">
            <div class="header-menu-product">
              <a href="ListProduct.jsp" class="header-menu-link">Product </a>
              <ion-icon
                name="chevron-down-outline"
                class="menu-down"
              ></ion-icon>
            </div>
            <div class="header-menu-child">
              <div class="menu-child-item">
                <a href="#"
                  ><ion-icon name="laptop-outline"></ion-icon>
                  Laptop
                </a>
              </div>
              <div class="menu-child-item">
                <a href="#"
                  ><i class="fas fa-desktop"></i>
                  PC
                </a>
              </div>
              <div class="menu-child-item">
                <a href="#"><i class="fas fa-microchip"></i> PC Components</a>
              </div>
              <div class="menu-child-item">
                <a href="#"
                  ><i class="far fa-keyboard"></i>
                  Keyboard
                </a>
              </div>
              <div class="menu-child-item">
                <a href="#"
                  ><i class="fas fa-mouse"></i>
                  Mouse
                </a>
              </div>
              <div class="menu-child-item">
                <a href="#"
                  ><i class="fas fa-headset"></i>
                  Head Phone
                </a>
              </div>
              <div class="menu-child-item">
                <a href="#"
                  ><i class="fas fa-chair"></i>
                  Chair
                </a>
              </div>
              <div class="menu-child-item">
                <a href="#"
                  ><i class="fas fa-mouse"></i>
                  Mouse
                </a>
              </div>
            </div>
          </li>
          <li class="header-menu-item">
            <a href="#" class="header-menu-link">Pricing</a>
          </li>
          <li class="header-menu-item">
            <a href="#" class="header-menu-link">About</a>
          </li>
        </ul>
        <div class="header-search">
          <input type="text" placeholder="Search" />
          <ion-icon name="search-outline" class="header-search-icon"></ion-icon>
        </div>
        <div class="header-cart">
          <a href="list_cart.jsp" class="cart-link">
            <ion-icon name="cart-outline"></ion-icon>
          </a>
        </div>
        <div class="header-auth">
          <a href="login.jsp" class="button button--header">Login</a>
          <a href="signup.jsp" class="button button--primary">Sign up</a>
        </div>
        <div class="header-user">
          <a href="#" class="header-user-image">
            <img src="./image/Logo.png" alt="">
          </a>
          <div class="header-user-info">
            <h3 class="header-user-name">Sang Ha</h3>
          </div>
          <div class="header-user-icon">
            <i class="fas fa-chevron-down"></i>
          </div>            
      </div>
