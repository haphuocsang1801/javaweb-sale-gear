<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="entity.User"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<a href="index.jsp" class="header-logo">
    <img src="./image/Logo.png" alt="" class="header-logo-image" />
</a>
<ul class="header-menu">
    <li class="header-menu-item">
        <a href="Home" class="header-menu-link">Home</a>
    </li>
    <form action="HomeController">
        <li class="header-menu-item hover">
        <div class="header-menu-product">
            <a href="product" class="header-menu-link">Product </a>
            <ion-icon name="chevron-down-outline" class="menu-down"></ion-icon>
        </div>
        <div class="header-menu-child">
            <div class="menu-child-item">
                <a href="#"
                   ><ion-icon name="laptop-outline"></ion-icon>
                    Laptop
                </a>
            </div>
            <div class="menu-child-item">
                <a href="#"
                   ><i class="fas fa-desktop"></i>
                    PC
                </a>
            </div>
            <div class="menu-child-item">
                <a href="#"><i class="fas fa-microchip"></i> PC Components</a>
            </div>
            <div class="menu-child-item">
                <a href="#"
                   ><i class="far fa-keyboard"></i>
                    Keyboard
                </a>
            </div>
            <div class="menu-child-item">
                <a href="#"
                   ><i class="fas fa-mouse"></i>
                    Mouse
                </a>
            </div>
            <div class="menu-child-item">
                <a href="#"
                   ><i class="fas fa-headset"></i>
                    Head Phone
                </a>
            </div>
            <div class="menu-child-item">
                <a href="#"
                   ><i class="fas fa-chair"></i>
                    Chair
                </a>
            </div>
        </div>
    </li>
    </form>
    <li class="header-menu-item">
        <a href="#" class="header-menu-link">About</a>
    </li>
    <%
        User loginUser = (User) session.getAttribute("LOGIN_USER");
        if(loginUser!=null && "AD".equals(loginUser.getRoleID().toUpperCase())){
                    %>
            <li class="header-menu-item">
                <a href="admin.jsp" class="header-menu-link">Admin</a>
            </li>
    <%               
        }
    %>
</ul>
<div class="header-search">
    <input type="text" placeholder="Search" />
    <button type="submit" name="action" value="submit"><ion-icon name="search-outline" class="header-search-icon"></ion-icon></button>
</div>
<div class="header-cart">
    <a href="list_card.jsp" class="cart-link">
        <ion-icon name="cart-outline"></ion-icon>
    </a>
</div>
<%
    if (loginUser == null) {
%>
<div class="header-auth">
    <a href="login.jsp" class="button button--header">Login</a>
    <a href="signup.jsp" class="button button--primary">Sign up</a>
</div>
<%
    } else if (loginUser != null) {
        %>
         <div class="header-user">
  <a href="#" class="header-user-image">
    <img src="./image/userr.jpg" alt="" />
    <div class="header-user-info">
      <h3 class="header-user-name"><%=loginUser.getFullname()%></h3>
    </div>
  </a>
  <a class="header-user-icon" href="logout" >
      <i class="fas fa-sign-out-alt">    
      </i>
  </a>
</div>      
<%
    }
%>


