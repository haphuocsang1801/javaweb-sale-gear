<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="entity.User"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<a href="Home" class="header-logo">
    <img src="./image/Logo.png" alt="" class="header-logo-image" />
</a>
<ul class="header-menu">
    <li class="header-menu-item">
        <a href="Home" class="header-menu-link">Home</a>
    </li>
    <form action="HomeController">
        <li class="header-menu-item hover">
        <div class="header-menu-product">
            <a href="product" class="header-menu-link">Product </a>
            <ion-icon name="chevron-down-outline" class="menu-down"></ion-icon>
        </div>
        <div class="header-menu-child">
            <c:forEach items="${sessionScope.Category}" var="o">
                  <div class="menu-child-item">
                    <a href="product?category=${o.id}">
                        ${o.name}
                    </a>
            </div>
            </c:forEach>
          
        </div>
    </li>
    </form>
    <li class="header-menu-item">
        <a href="#" class="header-menu-link">About</a>
    </li>
    <%
        User loginUser = (User) session.getAttribute("LOGIN_USER");
        if(loginUser!=null && "AD".equals(loginUser.getRoleID().toUpperCase())){
                    %>
            <li class="header-menu-item">
                <a href="admin.jsp" class="header-menu-link">Admin</a>
            </li>
    <%               
        }
    %>
</ul>
<form action="product">
    <div class="header-search">
        <input type="text" name="search-name" placeholder="Search" />
        <button type="submit" name="action" value="search" class="header-search-icon"><ion-icon name="search-outline" ></ion-icon></button>
    </div>
</form>
<div class="header-cart">
    <a href="list_cart.jsp" class="cart-link">
        <ion-icon name="cart-outline"></ion-icon>
    </a>
</div>
<%
    if (loginUser == null) {
%>
<div class="header-auth">
    <a href="login.jsp" class="button button--header">Login</a>
    <a href="signup.jsp" class="button button--primary" style="">Sign up</a>
</div>
<%
    } else if (loginUser != null) {
        %>
         <div class="header-user">
  <a href="userProfile.jsp" class="header-user-image">
    <img src="./image/userr.jpg" alt="" />
    <div class="header-user-info">
      <span class="header-user-name"><%=loginUser.getFullname()%></span>
    </div>
  </a>
  <a class="header-user-icon" href="logout" >
      <i class="fas fa-sign-out-alt">    
      </i>
  </a>
</div>      
<%
    }
%>


