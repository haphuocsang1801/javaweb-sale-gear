<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link
      href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;700&display=swap"
      rel="stylesheet"
    />
    <script
      src="https://kit.fontawesome.com/85150e2613.js"
      crossorigin="anonymous"
    ></script>
    <script
      type="module"
      src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"
    ></script>
    <script
      nomodule
      src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"
    ></script>
    <link
      href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
      rel="stylesheet"
    />
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="css/reset.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <title>Home Page</title>
  </head>
  <body>
    <div class="wrapper">
      <section class="header">
        <jsp:include page="header.jsp" />
      </section>
      <section class="slide">
        <jsp:include page="slide.jsp" />
      </section>
      <section class="product-list">
        <div class="container bootdey">
          <div class="col-md-12">
            <div class="row product-list">
              <div class="col-md-3">
                <section class="panel">
                  <div class="pro-img-box">
                    <img
                      src="https://via.placeholder.com/250x220/FFB6C1/000000"
                      alt=""
                    />
                    <a href="#" class="adtocart">
                      <i class="fa fa-shopping-cart"></i>
                    </a>
                  </div>

                  <div class="panel-body text-center">
                    <h4>
                      <a href="product_detail.jsp" class="pro-title">
                        Leopard Shirt Dress
                      </a>
                    </h4>
                    <p class="price">$300.00</p>
                  </div>
                </section>
              </div>

              <div class="col-md-3">
                <section class="panel">
                  <div class="pro-img-box">
                    <img
                      src="https://via.placeholder.com/250x220/FF7F50/000000"
                      alt=""
                    />
                    <a href="#" class="adtocart">
                      <i class="fa fa-shopping-cart"></i>
                    </a>
                  </div>

                  <div class="panel-body text-center">
                    <h4>
                      <a href="#" class="pro-title"> Leopard Shirt Dress </a>
                    </h4>
                    <p class="price">$300.00</p>
                  </div>
                </section>
              </div>
              <div class="col-md-3">
                <section class="panel">
                  <div class="pro-img-box">
                    <img
                      src="https://via.placeholder.com/250x220/3CB371/000000"
                      alt=""
                    />
                    <a href="#" class="adtocart">
                      <i class="fa fa-shopping-cart"></i>
                    </a>
                  </div>

                  <div class="panel-body text-center">
                    <h4>
                      <a href="#" class="pro-title"> Leopard Shirt Dress </a>
                    </h4>
                    <p class="price">$300.00</p>
                  </div>
                </section>
              </div>
              <div class="col-md-3">
                <section class="panel">
                  <div class="pro-img-box">
                    <img
                      src="https://via.placeholder.com/250x220/3CB371/000000"
                      alt=""
                    />
                    <a href="#" class="adtocart">
                      <i class="fa fa-shopping-cart"></i>
                    </a>
                  </div>

                  <div class="panel-body text-center">
                    <h4>
                      <a href="#" class="pro-title"> Leopard Shirt Dress </a>
                    </h4>
                    <p class="price">$300.00</p>
                  </div>
                </section>
              </div>
            </div>
            <section class="panel">
              <div class="panel-body">
                <div class="pull-right">
                  <ul class="pagination pagination-sm pro-page-list">
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">...</a></li>
                  </ul>
                </div>
              </div>
            </section>
          </div>
        </div>
      </section>
    </div>
    <section class="footer">
      <jsp:include page="footer.jsp" />
    </section>
  </body>
  <script src="./js/slide.js"></script>
</html>
