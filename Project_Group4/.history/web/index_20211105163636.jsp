<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link
            href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;700&display=swap"
            rel="stylesheet"
            />
        <script
            src="https://kit.fontawesome.com/85150e2613.js"
            crossorigin="anonymous"
        ></script>
        <script
            type="module"
            src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"
        ></script>
        <script
            nomodule
            src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"
        ></script>
        <link
            href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
            rel="stylesheet"
            />
        <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="css/reset.css" rel="stylesheet" type="text/css" />
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <title>Home Page</title>
    </head>
    <body>
        <jsp:useBean id="a" class="DAO.ProductDAO" scope="request"></jsp:useBean>
            <div class="wrapper">
                <section class="header">
                <jsp:include page="header.jsp" />
            </section>
            <c:if test="${requestScope.Show_home == 'home'}">
                <section class="slide">
                    <jsp:include page="slide.jsp" />
                </section>
                <section class="product-list">
                    <div class="container bootdey">
                        <div class="col-md-12">
                            <div class="container-product">
                                <div class="title-best">
                                    Top 5 best seller
                                </div>
                                <div class="row product-list">
                                    <c:forEach items="${requestScope.RESULT_SEARCH_PRODUCT}" var="o">
                                        <div class="col-md-3">
                                            <section class="panel product-item"  >
                                                <div class="pro-img-box">
                                                    <img
                                                        src="${o.image}"
                                                        alt="" class="product-item-image"
                                                        />
                                                    <a href="detail?action=${o.id}" class="adtocart">
                                                        <i class="fa fa-shopping-cart"></i>
                                                    </a>
                                                </div>
    
                                                <div class="panel-body text-center">
                                                    <h4>
                                                        <a href="detail?action=${o.id}" class="pro-title">
                                                            ${o.name}
                                                        </a>
                                                    </h4>
                                                    <p class="price">$ ${o.price}</p>
                                                </div>
                                            </section>
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>
                            <div class="container-product">
                                <div class="title-best">
                                    New product
                                </div>
                                <div class="row product-list">
                                    <c:forEach items="${requestScope.RESULT_SEARCH_PRODUCT}" var="o">
                                        <div class="col-md-3">
                                            <section class="panel product-item"  >
                                                <div class="pro-img-box">
                                                    <img
                                                        src="${o.image}"
                                                        alt="" class="product-item-image"
                                                        />
                                                    <a href="detail?action=${o.id}" class="adtocart">
                                                        <i class="fa fa-shopping-cart"></i>
                                                    </a>
                                                </div>
    
                                                <div class="panel-body text-center">
                                                    <h4>
                                                        <a href="detail?action=${o.id}" class="pro-title">
                                                            ${o.name}
                                                        </a>
                                                    </h4>
                                                    <p class="price">$ ${o.price}</p>
                                                </div>
                                            </section>
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>
                            <section class="panel">
                                <div class="panel-body">
                                    <div class="pull-right">
                                        <ul class="pagination pagination-sm pro-page-list">
                                            <c:forEach begin="1" end="${a.numberPage}" var="i">
                                                <li><a href="Home?indexPage=${i}">${i}</a></li>
                                            </c:forEach>
                                        </ul>
                                    </div>
                                </div>
                            </section>
                        </c:if>
                        <c:if test="${requestScope.Show_home == 'search'}">

                            <section class="product-list">
                                <div class="container bootdey">
                                    <div class="col-md-12">
                                        <h1>Search results for "${requestScope.name}".</h1>
                                        <div class="row product-list">
                                            <c:forEach items="${requestScope.RESULT_PRODUCT}" var="o">
                                                <div class="col-md-3">
                                                    <section class="panel product-item"  >
                                                        <div class="pro-img-box">
                                                            <img
                                                                src="${o.image}"
                                                                alt="" class="product-item-image"
                                                                />
                                                            <a href="detail?action=${o.id}" class="adtocart">
                                                                <i class="fa fa-shopping-cart"></i>
                                                            </a>
                                                        </div>

                                                        <div class="panel-body text-center">
                                                            <h4>
                                                                <a href="detail?action=${o.id}" class="pro-title">
                                                                    ${o.name}
                                                                </a>
                                                            </h4>
                                                            <p class="price">$ ${o.price}</p>
                                                        </div>
                                                    </section>
                                                </div>
                                            </c:forEach>
                                        </div>
                                        <section class="panel">
                                            <div class="panel-body">
                                                <div class="pull-right">
                                                    <ul class="pagination pagination-sm pro-page-list">
                                                        <c:forEach begin="1" end="${requestScope.RESULT_PRODUCT_LENGTH}" var="i">
                                                            <li><a href="product?index=${i}&action=search&search-name=${requestScope.name}">${i}</a></li>
                                                            </c:forEach>                              
                                                    </ul>
                                                </div>
                                            </div>
                                        </section>
                                    </c:if>
                                </div>
                            </div>
                        </section>
                    </div>
                    <section class="footer">
                        <jsp:include page="footer.jsp" />
                    </section>
                    </body>
                    <script src="./js/slide.js"></script>
                    </html>
