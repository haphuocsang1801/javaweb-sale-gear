$(document).ready(function(){
  $('.image-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite:true,
    autoPlay:true,
    autoplaySpeed:1500,
  }
  );
});