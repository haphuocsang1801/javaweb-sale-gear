$(document).ready(function(){
  $('.image-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite:true,
    autoplay:false,
    autoplaySpeed:1500,
  }
  );
});