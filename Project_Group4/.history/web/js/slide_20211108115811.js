$(document).ready(function(){
  $('.image-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite:true,
    autoplay:false,
    autoplaySpeed:1500,
    prevArrow:"<img class='a-left control-c prev slick-prev' src='YOUR LEFT ARROW IMAGE URL'>",
    nextArrow:"<img class='a-right control-c next slick-next' src='YOUR RIGHT ARROW IMAGE URL'>"
  }
  );
});