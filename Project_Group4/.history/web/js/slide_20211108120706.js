$(document).ready(function(){
  $('.image-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite:true,
    autoplay:false,
    autoplaySpeed:1500,
    prevArrow:"<button type='button' class='slick-prev pull-left'><i class='fa fa-angle-left' aria-hidden='true'></i></button>",
    nextArrow:"<button type='button' class='slick-next pull-right'> <ion-icon name="arrow-forward-outline"></ion-icon></button>"
  }
  );
});