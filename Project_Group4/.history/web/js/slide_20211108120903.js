$(document).ready(function(){
  $('.image-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite:true,
    autoplay:false,
    autoplaySpeed:1500,
    prevArrow:`<button type='button' class='slick-prev pull-left'><ion-icon name="arrow-back-outline"></ion-icon></button>",
    nextArrow:"<button type='button' class='slick-next pull-right'><i class='fa fa-angle-right' aria-hidden='true'></i></button>",
  }
  );
});