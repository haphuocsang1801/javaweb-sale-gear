<%-- 
    Document   : list_cartd
    Created on : Oct 4, 2021, 7:59:33 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">=
        <link
            href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;700&display=swap"
            rel="stylesheet"
            />
        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.1/dist/css/bootstrap.min.css"
            rel="stylesheet"
            />
            <script
            src="https://kit.fontawesome.com/85150e2613.js"
            crossorigin="anonymous"
        ></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.1/dist/js/bootstrap.bundle.min.js"></script>
        <script
            type="module"
            src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"
        ></script>
        <link rel="stylesheet" href="css/reset.css" />
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <title>JSP Page</title>
    </head>
    <body>
        <div class="wrapper">
            <section class="header">
                <jsp:include page="header.jsp"/> 
            </section>
            <section class="slide">
                <jsp:include page="card.jsp"/> 
            </section>        
        </div>
        <section class="footer">
            <jsp:include page="footer.jsp"/> 
        </section>
    </body>
</html>
