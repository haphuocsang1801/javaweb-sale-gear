<%-- 
    Document   : orderHistory
    Created on : Nov 5, 2021, 9:50:48 PM
    Author     : sanghpce150201
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
         <link
            href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;700&display=swap"
            rel="stylesheet"
            />
        <script
            src="https://kit.fontawesome.com/85150e2613.js"
            crossorigin="anonymous"
        ></script>
        <script
            type="module"
            src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"
        ></script>
        <script
            nomodule
            src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"
        ></script>

        <link href="css/reset.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <body>
            <div class="wrapper">
                <section class="header">
                    <jsp:include page="header.jsp"/> 
                </section>
                <section class="purchar">
                    <div class="purchar-container">
                        <table border="1" id="">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Name</th>
                                    <th>Quantity</th>
                                    <th>Total Price</th>
                                    <th>Purchase date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <img class="product-image-input" src="./image/userr.jpg" alt="">
                                    </td>
                                    <td>Laptop</td>
                                    <td>50</td>
                                    <td>2020</td>
                                    <td>2021-11-01</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </section>
                <section class="footer">
                    <jsp:include page="footer.jsp"/> 
                </section>
        </body>
        

    </body>
</html>
