<%-- 
    Document   : producerAdmin
    Created on : Oct 28, 2021, 4:30:48 PM
    Author     : admin
--%>

<%@page import="java.util.List"%>
<%@page import="entity.Producer"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
       <%
    List<Producer> list_Producer = (List<Producer>) request.getAttribute("LIST_PRODUCER");//list category
    if (list_Producer != null) {
        if (!list_Producer.isEmpty()) {
%>

<div class="category-list">
    <div class="category-top">
        <div class="product-add">
            <a href="ShowInputController?action=ShowInsertProducer"><i class="fas fa-plus"></i>Add Producer</a>  
        </div>      
        <div class="category-action">
            <form action="MainControllerProducer">
                <input type="text" name="search" class="product-input" 
                       placeholder="name"/>
                <input type="submit" value="Search" name="action" class="product-button"/>
            </form>           
        </div>
    </div>
    <div class="category-bottom" >         
        <table>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Update</th>
                <th>Delete</th>  
            </tr>
            <%
                for (Producer elem : list_Producer) {
            %>
            <form action="MainControllerProducer" method="get">
                <tr>
                    <td>
                        <%=elem.getNsx_id()%>
                        <input class="input-producer" type="hidden" name="id" value="<%=elem.getNsx_id()%>"/>
                    </td>
                    <td>
                        <input class="input-producer" type="text" name="name" value="<%=elem.getNsx_name()%>"/>

                    </td>                       
                    <td><input type="submit" name="action" value="Update" class="product-button"/></td>

                    <td><input type="submit" name="action" value="Delete" class="product-button"/></td>                               
                </tr>          
            </form>
            <%
                }
            %>
        </table>
    </div>    
</div>
<%
        }
    }
%>
    </body>
</html>
