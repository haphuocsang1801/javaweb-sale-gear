<%-- Document : productAdmin Created on : Oct 24, 2021, 9:43:40 AM Author :
admin --%> <%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <div class="product-admin">
      <div class="product-top">
        <input type="text" name="search_product" id="" class="product-input" />
        <a href="" class="product-add">Add</a>
      </div>
      <table>
        <tr>
          <th>ID</th>
          <th>Name of product</th>
          <th>Type</th>
          <th>Price</th>
          <th>Desc</th>
          <th>Producer</th>
          <th>Quantity</th>
        </tr>
        <tr>
          <td><input type="number" name="" class="product-id" id="" /></td>
          <td><input type="text" name="" class="product-name" id="" /></td>
          <td>
            <select name="" id="">
                <option value="">LAPTOP</option>
                <option value="">MOUSE</option>
                <option value="">CHAIR</option>
            </select>
          </td>
          <td>
              <input type="number" name="" class="product-price" id="">
          </td>
          <td>
              <textarea name="" id="" cols="30" rows="10"></textarea>
          </td>
          <td></td>
        </tr>
      </table>
    </div>
  </body>
</html>
