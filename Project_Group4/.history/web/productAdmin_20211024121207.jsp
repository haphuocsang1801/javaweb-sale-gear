<%-- Document : productAdmin Created on : Oct 24, 2021, 9:43:40 AM Author :
admin --%> <%@page import="java.util.ArrayList"%>
<%@page import="entity.Product"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Document</title>
    </head>
    <body>
        <div class="product-admin">
            <div class="product-top">
                <form action="Maintroller">
                    Search:<input type="text" name="name" id="" class="product-input" placeholder="name of product" />
                    <input type="submit" value="search-product">
                </form>
                <a href="" class="product-add">Add</a>
            </div>
            <table>
                <%
                    List<Product> list_product = (List<Product>) request.getAttribute("RESULT_SEARCH_PRODUCT");
                    if (list_product.isEmpty() || list_product == null) {
                        list_product = new ArrayList<Product>();
                    } else {
                %>
                <tr>
                    <th>ID</th>
                    <th>Name of product</th>
                    <th>Type</th>
                    <th>Price</th>
                    <th>Producer</th>
                    <th>Quantity</th>
                    <th>Description</th>
                </tr>
                <%
                    for (Product elem : list_product) {
                %>
                <tr>
                    <td><input type="number" name="" class="product-id" value="<%=elem.getId()%>"/></td>
                    <td><input type="text" name="" class="product-name" value="<%=elem.getName()%>" /></td>
                    <td>
                        <%=elem.getType_name()%>
                    </td>
                    <td>
                        <input type="number" name="" class="product-price" value="<%=elem.getPrice()%>">
                    </td>
                    <td>
                        <%=elem.getNsx_name()%>
                    </td>
                    <td>
                        <input type="number" min="1" name="" class="product-quantity" value="<%=elem.getQuantity()%>">
                    </td>
                    <td>
                        <textarea name="description" id="" cols="30" rows="1"></textarea>
                    </td>
                </tr>
                <%
                        }

                    }
                %>
            </table>
        </div>
    </body>
</html>
