<%-- Document : productAdmin Created on : Oct 24, 2021, 9:43:40 AM Author :
admin --%> <%@page import="java.util.ArrayList"%>
<%@page import="entity.Product"%>
<%@page import="java.util.List"%>
        <div class="product-admin">
            <div class="product-top">
                <form action="MainControllerProduct">
                    Search:<input type="text" name="name" class="product-input" placeholder="name of product" />
                    <input type="submit" name="action" value="search">
                </form>
                <a href="MainControllerProduct?action=insert" class="product-add">Add</a>
            </div>
            <table>
                <%
                    List<Product> list_product = (List<Product>) request.getAttribute("RESULT_SEARCH_PRODUCT");
                    if (list_product.isEmpty() || list_product == null) {
                        list_product = new ArrayList<>();
                    } else {
                %>
                <tr>
                    <th>ID</th>
                    <th>Name of product</th>
                    <th>Type</th>
                    <th>Image</th>
                    <th>Price</th>
                    <th>Producer</th>
                    <th>Quantity</th>
                    <th>Description</th>
                    <th>Update</th>
                    <th>Delete</th>
                </tr>
                <%
                    for (Product elem : list_product) {
                %>
                <form action="MainControllerProduct">
                    <tr>
                        <td>
                            <input type="hidden" name="id" class="product-id" value="<%=elem.getId().trim()%>"/>
                            <%=elem.getId().trim()%>
                        </td>
                        <td>
                            <input type="text" name="product-name" class="product-name" value="<%=elem.getName()%>" />
                        </td>
                        <td>
                            <%=elem.getType_name()%>
                        </td>
                        <td>
                            <img src="" alt="">
                        </td>
                        <td>
                            <input type="number" name="price" class="product-price" value="<%=elem.getPrice()%>">
                        </td>
                        <td>
                            <%=elem.getNsx_name()%>
                        </td>
                        <td>
                            <input type="number" min="1" name="quantity" class="product-quantity" value="<%=elem.getQuantity()%>">
                        </td>
                        <td>
                            <textarea name="description" id="" cols="30" rows="1"><%=elem.getDesc()%></textarea>
                        </td>
                        <td>
                            <input type="submit" value="Update" name="action">
                        </td>
                        <td>
                            <input type="submit" value="Delete" name="action">
                        </td>
                    </tr>
                </form>
                <%
                        }

                    }
                %>
            </table>
        </div>

