<%-- Document : productAdmin Created on : Oct 24, 2021, 9:43:40 AM Author :
admin --%> <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="java.util.ArrayList"%>
<%@page import="entity.Product"%>
<%@page import="java.util.List"%>
        <div class="product-admin">
            <div class="product-top">
                <form action="MainControllerProduct" class="product-controller">
                    <div class="title">Search:</div>
                    <input type="text" name="name" class="product-input product-search" placeholder="name of product" />
                    <input type="submit" name="action" value="search" class="product-button">
                </form>
                <div class="product-add">
                    <a href="ShowInputController?action=ShowinsertProduct" class="product-add">
                        <i class="fas fa-plus"></i>Add Product</a>
                </div>
            </div>
            <div style="overflow-x:auto;">
                <table>
                    <%
                        List<Product> list_product = (List<Product>) request.getAttribute("RESULT_SEARCH_PRODUCT");
                        if (list_product.isEmpty() || list_product == null) {
                            list_product = new ArrayList<>();
                        } else {
                    %>
                    <tr>
                        <th>ID</th>
                        <th>Name of product</th>
                        <th>Type</th>
                        <th>Image</th>
                        <th>Price</th>
                        <th>Producer</th>
                        <th>Quantity</th>
                        <th>Description</th>
                        <th>Update</th>
                        <th>Delete</th>
                    </tr>
            
                    <c:forEach items="${requestScope.RESULT_SEARCH_PRODUCT}" var="i">
                    
                    <form action="MainControllerProduct">
                        <tr>
                            <td>
                                <input type="hidden" name="id" class="product-id" value="${i.id}"/>
                                ${i.id}
                            </td>
                            <td>
                                <input class="input-producer" type="text" name="product-name" class="product-name" value="${i.name}" />
                            </td>
                            <td>
                                
                                <select class="input-producer" name="id_category" style="">
                                    <c:forEach items="${requestScope.LIST_CATEGORY}" var="o">
                                       
                                        <option value="${o.id}" ${i.type_id == o.name?"selected":""}>
                                            ${o.name}
                                        </option>
                                    </c:forEach>
                                    
                                </select>
                                
                            </td>
                            <td>
                                <img class="product-image-input" src="${i.image}" alt="">
                                 <input type="hidden" name="image"value="${i.image}"/>
                            </td>
                            <td>
                                <input class="input-producer" type="number" name="price" class="product-price" value="${i.price}">
                            </td>
                            <td>
                                <select class="input-producer" name="id_producer">
                                    <c:forEach items="${requestScope.LIST_PRODUCER}" var="o">
                                        <option value="${o.nsx_id}" ${o.nsx_name==i.nsx_name?"selected":""}>
                                            ${o.nsx_name}
                                        </option>
                                    </c:forEach>
                                    
                                </select>
                            </td>
                            <td>
                                <input class="input-producer" type="number" min="0" name="quantity" class="product-quantity" value="${i.quantity}">
                            </td>
                            <td>
                                <textarea name="description" id="" cols="30" rows="1">${i.desc}</textarea>
                            </td>
                            <td>
                                <input type="submit" value="Update" name="action" class="product-button">
                            </td>
                            <td>
                                <input type="submit" value="Delete" name="action"  class="product-button">
                            </td>
                        </tr>
                    </form>
                            </c:forEach>
                    <%
    
                        }
                    %>
                </table>
            </div>
        </div>

