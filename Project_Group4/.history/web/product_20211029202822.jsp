<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link
  href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
  rel="stylesheet"
/>
<div class="container bootdey">
  <div class="col-md-3">
    <section class="panel">
      <header class="panel-heading">Category</header>
      <div class="panel-body">
        <ul class="nav prod-cat">
          <c:forEach items="${requestScope.RESULT_SEARCH_CATEGOGY}" var="o">
            <li>
              <a href="#" class="">
                <i class="fa fa-angle-right"></i>
                ${o.name}
              </a>
            </li>
          </c:forEach>
        </ul>
      </div>
    </section>
    <section class="panel">
      <header class="panel-heading">Price Range</header>
      <div class="panel-body sliders">
        <div id="slider-range" class="slider"></div>
        <div class="slider-info">
          <span id="slider-range-amount"></span>
        </div>
      </div>
    </section>
    <section class="panel">
      <header class="panel-heading">Filter</header>
      <div class="panel-body">
        <form role="form product-form">
          <div class="form-group">
            <label>Brand</label>
            <select name="" id="">
                <c:forEach items="${requestScope.RESULT_SEARCH_Producer}" var="o">
                    <option value="${o.nsx_id}">${o.nsx_name}</option>
                </c:forEach>
            </select>
          </div>
          <div class="form-group">
            <label>Category</label>
            <select name="" id="" class="">
                <c:forEach items="${requestScope.RESULT_SEARCH_CATEGOGY}" var="o">
                    <option value="${o.id}">${o.name}</option>
                </c:forEach>
            </select>
          </div>
          <button class="btn btn-primary" type="submit">Filter</button>
        </form>
      </div>
    </section>
  </div>
  <div class="col-md-9">
    <div class="row product-list">
      <c:forEach items="${requestScope.RESULT_SEARCH_PRODUCT}" var="o">
        <div class="col-md-4">
          <section class="panel">
            <div class="pro-img-box">
              <img src="${o.image}" alt="" />
              <a href="#" class="adtocart">
                <i class="fa fa-shopping-cart"></i>
              </a>
            </div>

            <div class="panel-body text-center">
              <h4>
                <a href="product_detail.jsp" class="pro-title"> ${o.name} </a>
              </h4>
              <p class="price">$${o.price}</p>
            </div>
          </section>
        </div>
      </c:forEach>
    </div>
    <section class="panel">
      <div class="panel-body">
        <div class="pull-right">
          <ul class="pagination pagination-sm pro-page-list">
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">...</a></li>
          </ul>
        </div>
      </div>
    </section>
  </div>
</div>
