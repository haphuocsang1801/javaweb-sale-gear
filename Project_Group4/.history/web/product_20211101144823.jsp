<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link
    href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
    rel="stylesheet"
    />
<jsp:useBean id="a" class="DAO.ProductDAO" scope="request"></jsp:useBean>
    <div class="container bootdey">
        <div class="col-md-3">
            <section class="panel">
                <header class="panel-heading">Category</header>
                <div class="panel-body">
                    <ul class="nav prod-cat">
                    <c:forEach items="${requestScope.RESULT_CATEGOGY}" var="o">
                        <li>
                            <a href="product?category=${o.id}" class="${requestScope.tag == o.id?"active":""}">
                                <i class="fa fa-angle-right"></i>
                                ${o.name}
                            </a>
                        </li>
                    </c:forEach>
                </ul>
            </div>
        </section>
        <section class="panel">
            <header class="panel-heading">Filter</header>
            <div class="panel-body">
                <form role="form product-form">
                    <div class="form-group">
                        <label>Brand</label>
                        <select name="" id="" class="product-filter">
                            <c:forEach items="${requestScope.RESULT_Producer}" var="o">
                                <option value="${o.nsx_id}">${o.nsx_name}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Category</label>
                        <select name="" id="" class="product-filter">
                            <c:forEach items="${requestScope.RESULT_CATEGOGY}" var="o">
                                <option value="${o.id}">${o.name}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Price</label>
                        input
                    </div>
                    <button class="btn btn-primary" type="submit">Filter</button>
                </form>
            </div>
        </section>
    </div>

    <div class="col-md-9">
        <c:if test="${not empty requestScope.NOTFOUND}">
            <h1>${requestScope.NOTFOUND}</h1>
        </c:if>
        <c:if test="${empty requestScope.NOTFOUND }">
            <div class="row product-list">

                <c:forEach items="${requestScope.RESULT_PRODUCT}" var="o">
                    <div class="col-md-4 " >
                        <section class="panel product-item  ">
                            <div class="pro-img-box">
                                <img src="${o.image}" alt="" class="product-item-image"/>
                                <a href="#" class="adtocart">
                                    <i class="fa fa-shopping-cart"></i>
                                </a>
                            </div>

                            <div class="panel-body text-center">
                                <h4>
                                    <a href="detail?action=${o.id}" class="pro-title"> ${o.name} </a>
                                </h4>
                                <p class="price">$${o.price}</p>
                            </div>
                        </section>
                    </div>
                </c:forEach>
            </div>
            <section class="panel">
                <div class="panel-body">
                    <div class="pull-center" style="text-align: right;">
                        <ul class="pagination pagination-sm pro-page-list">    
                            <c:if test="${not empty requestScope.PageCategory}">
                                <c:forEach begin="1" end="${requestScope.RESULT_PRODUCT_LENGTH}" var="i">
                                    <li><a href="product?indexPage=${i}&category=${requestScope.CategoryID}">${i}</a></li>
                                    </c:forEach>
                                </c:if>
                                <c:if test="${empty requestScope.PageCategory}">
                                    <c:forEach begin="1" end="${a.numberPage}" var="i">
                                    <li><a href="product?indexPage=${i}">${i}</a></li>
                                    </c:forEach>
                                </c:if>
                        </ul>
                    </div>
                </div>
            </section>
        </c:if>
    </div>
</div>
