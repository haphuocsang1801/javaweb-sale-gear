<%-- 
    Document   : product_detail
    Created on : Oct 4, 2021, 3:51:20 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link href="css/reset.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <title>JSP Page</title>
    </head>
    <body>
        <div class="wrapper">
            <section class="header">
                <jsp:include page="header.html"/> 
            </section>
            <section class="product-list">
                <jsp:include page="product_detail.html"/> 
            </section>        
        </div>
        <section class="footer">
            <jsp:include page="footer.html"/> 
        </section>
    </body>
</html>
