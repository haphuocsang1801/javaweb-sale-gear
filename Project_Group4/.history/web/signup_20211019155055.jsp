<%@page import="entity.UserError"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link
            href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;700&display=swap"
            rel="stylesheet"
            />
        <script
            src="https://kit.fontawesome.com/85150e2613.js"
            crossorigin="anonymous"
        ></script>
        <script
            type="module"
            src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"
        ></script>
        <script
            nomodule
            src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"
        ></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <link rel="stylesheet" href="./css/reset.css" />
        <link rel="stylesheet" href="./css/style.css" />
        <title>Sigup</title>
    </head>

    <body>
        <%
            UserError userError=(UserError) request.getAttribute("USER_SIGUP");
            if(userError==null){
                 userError=new UserError();
            }
        %>
        <div class="wrapper">
            <section class="header">
                <jsp:include page="header.jsp"/> 
            </section>    
            <section class="login">
                <form action="MainController" method="post" autocomplete="off">
                    <div class="login-main">
                        <div class="login-title">
                            Sign up.
                        </div>
                        <div class="login-field">
                            <i class="fa fa-user-lock login-icon">
                            </i>
                            <input type="text" class="login-input" placeholder="Username" name="username" required="">
                            <div class="error">
                                p
                            </div>
                            <%= userError.getUserNameError() %>
                        </div>
                        <div class="login-field">
                            <i class="fa fa-user-alt login-icon">
                            </i>
                            <input type="text" class="login-input" placeholder="Fullname" name="fullname" required="">
                            <%= userError.getFullnameError()%>
                        </div>
                        <div class="login-field">
                            <i class="fa fa-envelope login-icon">
                            </i>
                            <input type="text" class="login-input" placeholder="Your email" name="email" required="">
                            <%= userError.getEmailError()%>
                        </div>
                        <div class="login-field">
                            <i class="fa fa-phone login-icon">
                            </i>
                            <input type="text" class="login-input" placeholder="Your phone" name="numberphone" required="">
                            <%= userError.getPhoneError()%>
                        </div>
                        <div class="login-field">
                            <i class="fa fa-lock login-icon">
                            </i>
                            <input type="password" class="login-input" placeholder="Your password" name="password" required="">
                            <%= userError.getPasswordError()%>
                        </div>
                        <div class="login-field">
                            <i class="fa fa-home login-icon">
                            </i>
                            <input type="text" class="login-input" placeholder="Address" name="address" required="">
                            <%= userError.getAddressError()%>
                        </div>
                        <div class="sigup-button">
                            <button class="login-submit login--create" name="action" value="sigup">Create account</button>
                            <a href="login.jsp" class="login-submit login--back">Back</a>
                        </div>
                        <%= userError.getMessageError()%>
                    </div>
                </form>      
            </section>
        </div>
            <section class="footer">
                 <jsp:include page="footer.jsp"/> 
            </section>
    </body>
    
</html>