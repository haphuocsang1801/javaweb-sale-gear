
    <div class="slider">
      <i class="fa fa-arrow-left slider-prev"></i>
      <ul class="slider-dot">
        <li class="slider-dot-item active" data-index="0"></li>
        <li class="slider-dot-item" data-index="1"></li>
        <li class="slider-dot-item" data-index="2"></li>
        <li class="slider-dot-item" data-index="3"></li>
        <li class="slider-dot-item" data-index="4"></li>
      </ul>
        <div class="slider-wrapper">
          <div class="slider-main">
              <div class="slider-item">
                <img
                  src="https://images.unsplash.com/photo-1632104009230-4c927433490f?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=2000&q=80"
                  alt=""
                />
              </div>
              <div class="slider-item">
                <img
                  src="https://images.unsplash.com/photo-1583337130417-3346a1be7dee?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1000&q=80"
                  alt=""
                />
              </div>
              <div class="slider-item">
                <img
                  src="https://images.unsplash.com/photo-1583337130417-3346a1be7dee?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1000&q=80"
                  alt=""
                />
              </div>
              <div class="slider-item">
                <img
                  src="https://images.unsplash.com/photo-1632104009230-4c927433490f?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=2000&q=80"
                  alt=""
                />
              </div>
              <div class="slider-item">
                <img
                  src="https://images.unsplash.com/photo-1583337130417-3346a1be7dee?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1000&q=80"
                  alt=""
                />
              </div>
            </div>
        </div>
      <i class="fa fa-arrow-right slider-next"></i>
    </div>
    <script src="./js/slide.js"></script>
