<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/reset.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <style>
        </style>
    </head>
    <body>
        <div class="slideshow-container">
            <c:forEach items="${requestScope.RESULT_SEARCH_PRODUCT}" var="o">
                <div class="mySlides fade">
                    <img src="${o.image}" class="slide-img">
                </div>
            </c:forEach>

        </div>
        <br>

        <div style="text-align:center">
            <c:forEach items="${requestScope.RESULT_SEARCH_PRODUCT}" var="o">
                <span class="dot"></span> 
            </c:forEach>
        </div>
        <script src="js/slide.js" type="text/javascript"></script>
    </body>
</html> 