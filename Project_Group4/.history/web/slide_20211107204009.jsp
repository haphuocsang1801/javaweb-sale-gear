<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/reset.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="http://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
        <style>
        </style>
    </head>
    <body>
        <div class="slideshow-container">
            <c:forEach items="${requestScope.RESULT_SEARCH_PRODUCT_TOP5}" var="o">
                <div class="mySlides fade">
                    <a href="detail?action=${o.id}">
                        <img src="${o.image}" class="slide-img">
                    </a>
                </div>
            </c:forEach>

        </div>
        <br>

        <div style="text-align:center">
                <span class="dot"></span> 
                <span class="dot"></span> 
                <span class="dot"></span> 
                <span class="dot"></span> 
                <span class="dot"></span> 
                <span class="dot"></span> 
        </div>
        <script src="js/slide.js" type="text/javascript"></script>
    </body>
</html> 