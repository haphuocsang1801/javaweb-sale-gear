<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/reset.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    </head>
    <body>
        <div class="slideshow-container">
            <!-- <c:forEach items="${requestScope.RESULT_SEARCH_PRODUCT_TOP5}" var="o">
                <div class="mySlides">
                    <a href="detail?action=${o.id}">
                        <img src="${o.image}" class="slide-img" >
                    </a>
                </div>
            </c:forEach> -->
            <div class="mySlides">
                <a href="#" class="image">
                    <img src="" alt="">
                </a>
            </div>
        </div>
        <script src="js/slide.js" type="text/javascript"></script>
    </body>
</html> 