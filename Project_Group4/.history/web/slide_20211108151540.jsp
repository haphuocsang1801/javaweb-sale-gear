<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/reset.css" rel="stylesheet" type="text/css"/>
        <script
            type="module"
            src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"
        ></script>
        <script
            nomodule
            src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"
        ></script>
        <link
        rel="stylesheet"
        type="text/css"
        href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"
      />
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
       
    </head>
    <body>
        <div class="image-slider">
            <!-- <c:forEach items="${requestScope.RESULT_SEARCH_PRODUCT_TOP5}" var="o">
                <div class="mySlides">
                    <a href="detail?action=${o.id}">
                        <img src="${o.image}" class="slide-img" >
                    </a>
                </div>
            </c:forEach> -->
                        <div class="image-item">
                <a href="#" class="image">
                    <img src="https://images.unsplash.com/photo-1636269734154-b350b6631f4b?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=687&q=80" alt="">
                </a>
            </div>
        </div>

        <script
          type="text/javascript"
          src="https://code.jquery.com/jquery-1.11.0.min.js"
        ></script>
        <script
          type="text/javascript"
          src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"
        ></script>
        <script
          type="text/javascript"
          src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"
        ></script>
        <script src="js/slide.js" type="text/javascript"></script>
    </body>
</html> 