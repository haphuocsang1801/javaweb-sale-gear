<%-- 
    Document   : userProfile
    Created on : Nov 5, 2021, 7:43:59 PM
    Author     : sanghpce150201
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link
            href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;700&display=swap"
            rel="stylesheet"
            />
        <script
            src="https://kit.fontawesome.com/85150e2613.js"
            crossorigin="anonymous"
        ></script>
        <script
            type="module"
            src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"
        ></script>
        <script
            nomodule
            src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"
        ></script>

        <link href="css/reset.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="wrapper">
            <section class="header">
                <jsp:include page="header.jsp"/> 
            </section>
            <section class="profile">
                <div class="profile-container">
                    <div class="profile-left">
                        <img src="./image/userr.jpg" alt="">
                        <input type="text" class="profile-name" value="sang">
                        <a href="#" class="profile-purchar">Order history</a>
                    </div>
                    <div class="profile-right">
                        <div class="profile-header">
                            Information
                        </div>
                        .profile-
                    </div>
                </div>
            </section>
            <section class="footer">
                <jsp:include page="footer.jsp"/> 
            </section>
    </body>
</html>
