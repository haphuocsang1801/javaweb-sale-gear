<%-- 
    Document   : userProfile
    Created on : Nov 5, 2021, 7:43:59 PM
    Author     : sanghpce150201
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link
            href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;700&display=swap"
            rel="stylesheet"
            />
        <script
            src="https://kit.fontawesome.com/85150e2613.js"
            crossorigin="anonymous"
        ></script>
        <script
            type="module"
            src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"
        ></script>
        <script
            nomodule
            src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"
        ></script>

        <link href="css/reset.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="wrapper">
            <section class="header">
                <jsp:include page="header.jsp"/> 
            </section>
            <form action="MainController">
                <c:set var="o" value="${sessionScope.LOGIN_USER}"></c:set>
            <section class="profile">
                <div class="profile-container">
                    <div class="profile-left">
                        <img src="./image/userr.jpg" alt="">
                        ${o.fullname}
                        <span class="profile-left-role">Admin</span>
                    </div>
                    <div class="profile-right">
                        <div class="profile-header">
                            Information
                        </div>
                        <div class="profile-detail">
                            <div class="profile-item">
                                <span>Email</span>
                                <input type="email" class="profile-input" name="email" value="${o.email}">
                            </div>
                            <div class="profile-item">
                                <span>Phone</span>
                                <input type="number" class="profile-input" name="phone" value="${o.phone}">
                            </div>
                            <div class="profile-item">
                                <span>Address</span>
                                <input type="text" class="profile-input" name="address" value="${o.address}">
                            </div>
                        </div>
                            <input type="hidden" name="id" value="${o.userID}"/>
                        <button type="submit" name="action" value="save" class="profile-save">Save</button>
                                ${requestScope.SAVE}
                        <div class="profile-footer">
                            <a href="or" class="profile-purchar"><i class="fas fa-history"></i>
                                Order history</a>
                            <a href="#" class="profile-purchar"><i class="fas fa-key"></i>
                                Change password</a>
                        </div>
                    </div>
                </div>
            </section>
            </form>
            <section class="footer">
                <jsp:include page="footer.jsp"/> 
            </section>
    </body>
</html>
