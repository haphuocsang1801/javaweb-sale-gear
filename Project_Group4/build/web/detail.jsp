
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="container bootdey">

    <div class="col-md-12">
        <form action="CartController">
            <section class="panel">
                <div class="panel-body">
                    <c:set var="o" value="${requestScope.PRODUCT_DETAIL}" />
                    <div class="col-md-6">
                        <div class="pro-img-details">
                            <img
                                src="${o.image}"
                                alt=""
                                />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h4 class="pro-d-title">
                            <a href="#" class="">${o.name}</a>
                        </h4>
                        <p class="footer-desc">
                            ${o.desc}
                        </p>
                        <div class="product_meta">
                            <span class="tagged_as"
                                  ><strong>Producer:</strong> <a rel="tag" href="#">${o.nsx_name}</a>
                        </div>
                        <div class="m-bot15">
                            <strong>Price : </strong>
                            <span class="pro-price"> $${o.price}</span>
                        </div>
                        <div class="form-group">
                            <label>Quantity</label>
                            <input
                                type="number" min="1"
                                placeholder="1"
                                class="form-control quantity" name="quantity" value="1"
                                />
                        </div>
                        <input type="hidden" name="id-product" value="${o.id}"/>
                        <input type="hidden" name="price-product" value="${o.price}"/>
                        <p>
                            <button class="button button--primary" style="border: none;
                                    " type="submit" name="action" value="Add to cart">
                                <i class="fa fa-shopping-cart"></i> Add to Cart
                            </button>
                        </p>
                        <div class="product-mess">${requestScope.ADD_SUCCESS}</p>
                        </div>
                    </div>
            </section>
        </form>
    </div>
</div>
<script type="text/javascript"></script>
