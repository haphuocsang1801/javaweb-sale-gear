<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link
            href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;700&display=swap"
            rel="stylesheet"
            />
        <script
            src="https://kit.fontawesome.com/85150e2613.js"
            crossorigin="anonymous"
        ></script>
        <script
            type="module"
            src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"
        ></script>
        <script
            nomodule
            src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"
        ></script>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <link rel="stylesheet" href="./css/reset.css" />
        <link rel="stylesheet" href="./css/style.css" />
        <title>Login</title>
    </head>
    <body>
        <div class="wrapper">
            <section class="header">
                <jsp:include page="header.jsp"/> 
            </section>
            <section class="login">
                <form action="MainController" method="get" autocomplete="off">
                    <div class="login-main">
                        <div class="login-title">Log in.</div>
                        <div class="login-field">
                            <i class="fa fa-envelope login-icon"> </i>
                            <input
                                type="text"
                                class="login-input" name="username"
                                placeholder="Enter your email"
                                />
                        </div>
                        <div class="login-field">
                            <i class="fa fa-lock login-icon"> </i>
                            <input type="password" class="login-input" placeholder="Password" name="password" />
                        </div>
                        <c:if test="${requestScope.ERROR_MASSAGE != null}">
                            <div class="dangerous" >
                                <i class="fas fa-exclamation"></i>Incorrect user name or password
                            </div>
                        </c:if>
                        <a href="#" class="login-forgot-password">Forgot password?</a>
                        <button class="login-submit" value="login" name="action">Login</button>
                    </div>
                </form>
            </section>
        </div>
        <footer class="footer footer--login">
            <jsp:include page="footer.jsp"/> 
        </footer>
    </body>
</html>
