<%@page import="entity.User"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="entity.UserError"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link
            href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;700&display=swap"
            rel="stylesheet"
            />
        <script
            src="https://kit.fontawesome.com/85150e2613.js"
            crossorigin="anonymous"
        ></script>
        <script
            type="module"
            src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"
        ></script>
        <script
            nomodule
            src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"
        ></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <link rel="stylesheet" href="./css/reset.css" />
        <link rel="stylesheet" href="./css/style.css" />
        <title>Sigup</title>
    </head>

    <body>
        <c:set var="object" value="${requestScope.USER_SIGUP}" />
        <div class="wrapper">
            <section class="header">
                <jsp:include page="header.jsp"/> 
            </section>    
            <section class="login">
                <form action="MainController" method="post" autocomplete="off">
                    <div class="login-main">
                        <div class="login-title">
                            Sign up.
                        </div>
                        <div class="login-field">
                            <i class="fa fa-user-lock login-icon">
                            </i>
                            <input type="text" class="login-input" placeholder="Username" name="username" required=""> 

                        </div>
                        <c:if test="${not empty object.userNameError}">
                             <div class="dangerous" >
                                <i class="fas fa-exclamation"></i>
                                ${object.userNameError}
                            </div>
                        </c:if>

                        <div class="login-field">
                            <i class="fa fa-user-alt login-icon">
                            </i>
                            <input type="text" class="login-input" placeholder="Fullname" name="fullname" required="">                        
                        </div>
                        <c:if test="${not empty object.fullnameError}">
                             <div class="dangerous" >
                                <i class="fas fa-exclamation"></i>
                                Full name length must be 5-25 charaters
                            </div>
                        </c:if>
                        <div class="login-field">
                            <i class="fa fa-envelope login-icon">
                            </i>
                            <input type="text" class="login-input" placeholder="Your email" name="email" required="">                           
                        </div>
                        <c:if test="${ not empty object.emailError }">
                             <div class="dangerous" >
                                <i class="fas fa-exclamation"></i>
                               Email is not validating
                            </div>
                        </c:if>
                        <div class="login-field">
                            <i class="fa fa-phone login-icon">
                            </i>
                            <input type="text" class="login-input" placeholder="Your phone" name="numberphone" required="">
                        </div>
                        <c:if test="${not empty object.phoneError }">
                             <div class="dangerous" >
                                <i class="fas fa-exclamation"></i>
                               Phone number must be 10 to 11 numbers
                            </div>
                        </c:if>
                        <div class="login-field">
                            <i class="fa fa-lock login-icon">
                            </i>
                            <input type="password" class="login-input" placeholder="Your password" name="password" required="">
                        </div>
                        <c:if test="${not empty object.passwordError}">
                             <div class="dangerous" >
                                <i class="fas fa-exclamation"></i>
                               Password length must be 5 to 30 characters
                            </div>
                        </c:if>
                        <div class="login-field">
                            <i class="fa fa-home login-icon">
                            </i>
                            <input type="text" class="login-input" placeholder="Address" name="address" required="">
                        </div>
                        <c:if test="${not empty object.addressError}">
                             <div class="dangerous" >
                                <i class="fas fa-exclamation"></i>
                                Address length must be 5 to 30 characters
                            </div>
                        </c:if>
                        <div class="sigup-button">
                            <button class="login-submit login--create" name="action" value="sigup">Create account</button>
                            <a href="login.jsp" class="login-submit login--back">Back</a>
                        </div>
                        <c:if test="${not empty object.messageError}">
                             <div class="dangerous" >
                                <i class="fas fa-exclamation"></i>
                                Username already exists
                            </div>
                        </c:if>
                    </div>
                </form>      
            </section>
        </div>
        <section class="footer">
            <jsp:include page="footer.jsp"/> 
        </section>
    </body>

</html>