/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.Cart;

import DAO.CartDAO;
import entity.Cart;
import entity.CartItems;
import entity.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author sanghpce150201
 */
@WebServlet(name = "CheckOutCartController", urlPatterns = {"/CheckOutCartController"})
public class CheckOutCartController extends HttpServlet {

    private final static String ERROR = "list_cart.jsp";
    private final static String SUCCESS = "list_cart.jsp";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute("USER_LOGIN");
            Cart cart = (Cart) session.getAttribute("CART");
            CartDAO dao = new CartDAO();
            boolean checkInsert = false;
            boolean minusQuantity = false;

            boolean checkEnough = false;//use to check product enough?
            for (CartItems object : cart.getCart().values()) {
                String id_product = object.getProduct().getId();
                int quantity = object.getQuanity_cart();
                if (dao.getQuantityProduct(id_product) - quantity < 0) {
                    checkEnough = true;
                }
            }
            if (checkEnough) {
                request.setAttribute("Out_of_stock", "Out of stock");
            } else {
                for (CartItems object : cart.getCart().values()) {
                    String product_id = object.getProduct().getId();
                    int quantity = object.getQuanity_cart();
                    float price = object.getPrice_cart();
                    int user_id = object.getUserID();
                    checkInsert = dao.insertCart(user_id,quantity , price, product_id);//insert in to order
                    minusQuantity = dao.minusQuantity(quantity, product_id);//minus quantity of product      
                    boolean exist = dao.checkExits(product_id);
                    if (exist) {
                          dao.updateSoldProduct(product_id, quantity);//if product already exits then to update quantity
                    }
                    else{
                        dao.insertSoldProduct(product_id, quantity);//if product not already exits then to insert product and quantity
                    }
                }
            }

            if (!checkEnough) {
                session.setAttribute("CART", null);
                request.setAttribute("CHECKOUT", "Payment success");
                url = SUCCESS;
            }
        } catch (Exception e) {
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
