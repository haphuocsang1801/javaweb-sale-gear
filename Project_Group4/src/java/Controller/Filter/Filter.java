/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.Filter;

import DAO.CategoryDAO;
import DAO.ProducerDAO;
import DAO.ProductDAO;
import entity.Category;
import entity.Producer;
import entity.Product;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author sanghpce150201
 */
@WebServlet(name = "Filter", urlPatterns = {"/Filter"})
public class Filter extends HttpServlet {

    private final static String ERROR = "ListProduct.jsp";
    private final static String SUCCESS = "ListProduct.jsp";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            ProductDAO productDao = new ProductDAO();
            CategoryDAO cateDao = new CategoryDAO();
            ProducerDAO producerDAO = new ProducerDAO();

            List<Producer> listProducer = producerDAO.getListProducer("");
            List<Category> listCate = cateDao.getCategory("");

            String nsx_id = request.getParameter("nsx-id");
            if(nsx_id==null){
                nsx_id="";
            }
            String category_id = request.getParameter("category-id");
            if(category_id==null){
                category_id="";
            }
            float minPrice = Float.parseFloat(request.getParameter("min-price"));
            float maxPrice = Float.parseFloat(request.getParameter("max-price"));          
            String index = request.getParameter("indexPage");
            if (index == null) {
                index = "1";
            }
            int count = productDao.countProductFilter(nsx_id, category_id, minPrice, maxPrice);
            List<Product> listProduct = productDao.get9ProductByFilter(Integer.parseInt(index), nsx_id, category_id, minPrice, maxPrice);
            if (listProduct.isEmpty()) {
                request.setAttribute("NOTFOUND", "No products were found");
            }
            request.setAttribute("RESULT_PRODUCT", listProduct);
            request.setAttribute("RESULT_CATEGOGY", listCate);
            request.setAttribute("RESULT_Producer", listProducer);
            request.setAttribute("RESULT_PRODUCT_LENGTH", count);
            request.setAttribute("PageCategory", "false");

        } catch (Exception e) {
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
