/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.CategoryDAO;
import DAO.ProductDAO;
import entity.Category;
import entity.Product;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author sanghpce150201
 */
public class HomeController extends HttpServlet {

    private final static String ERROR = "index.jsp";
    private final static String SUCCESS = "index.jsp";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            List<Product> list = new ArrayList<>();
            List<Product> listTop5 = new ArrayList<>();
            List<Category> listCate = new ArrayList<>();
            
            CategoryDAO daoCate = new CategoryDAO();
            listCate = daoCate.getCategory("");
            HttpSession session = request.getSession();
            session.setAttribute("Category", listCate);
            
            ProductDAO dao = new ProductDAO();
            String indexPage = request.getParameter("indexPage");
            if (indexPage == null) {
                indexPage = "1";
            }
            list = dao.get12ProductByPage(Integer.parseInt(indexPage));
            List<Product> listTop5Newest = dao.getTop5Product();
            List<String> listValue = dao.top5Best();
            for (String string : listValue) {
                listTop5.add(dao.searchProductByID(string.toString()));
            }
            if (list.isEmpty()) {
                request.setAttribute("NOT_FOUND", "There are no new products");
            } else {
                request.setAttribute("Show_home", "home");
                request.setAttribute("RESULT_SEARCH_PRODUCT", list);
                request.setAttribute("RESULT_SEARCH_PRODUCT_TOP5", listTop5Newest);
                request.setAttribute("RESULT_SEARCH_PRODUCT_TOP5_BEST", listTop5);
                url = SUCCESS;
            }

        } catch (Exception e) {
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
