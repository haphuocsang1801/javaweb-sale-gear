/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author admin
 */
public class MainController extends HttpServlet {
    private static final String ERORR="Home";
    private static final String LOGIN="Login_Controller";
    private static final String SIGUP="SigupController";
    private static final String SHOW_CATEGORY="ShowController";
    private static final String UPDATE_CATEGORY="UpdateController";
    private static final String DELETE_CATEGORY="DeleteController";
    private static final String INSERT_CATEGORY="InsertCaterogyController";
    private static final String SEARCH_CATEGORY="SearchCaterogyController";
    private static final String FILTER="Filter";
    private static final String SAVE_USER="SaveInformationController";
    private static final String HISTORY="HistoryPurchaseController";
    private static final String CHANGEPASSWORD="ChangePasswordController";
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url=ERORR;
        try {
            String action=request.getParameter("action");
           
            if("login".equals(action)){
                url=LOGIN;
            }
            else if("sigup".equals(action)){
                url=SIGUP;
            }
            else if("Show_category".equals(action)){
                url=SHOW_CATEGORY;
            }
            else if("Update-Category".equals(action)){
                url=UPDATE_CATEGORY;
            }
            else if("Delete-Category".equals(action)){
                url=DELETE_CATEGORY;
            } 
            else if("Insert".equals(action)){
                url=INSERT_CATEGORY;
            }
            else if("search_category".equals(action) || "Search".equals(action)){  
                url=SEARCH_CATEGORY;
            }   
            else if("filter".equals(action)){
                url=FILTER;
            }
            else if("save".equals(action)){
                url=SAVE_USER;
            }
            else if("history".equals(action)){
                url=HISTORY;
            }
            else if("changePassword".equals(action)){
                url=CHANGEPASSWORD;
            }
            else{
                url=ERORR;
            }
            
        } catch (Exception e) {
            log("Error at MainController"+e.toString());
        }finally{
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
