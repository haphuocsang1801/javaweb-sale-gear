/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.Product;

import DAO.CategoryDAO;
import DAO.ProducerDAO;
import DAO.ProductDAO;
import entity.Category;
import entity.Producer;
import entity.Product;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author sanghpce150201
 */
@WebServlet(name = "ProductController", urlPatterns = {"/product"})
public class ProductController extends HttpServlet {
    
    private final static String ERROR = "ListProduct.jsp";
    private final static String SUCCESS = "ListProduct.jsp";
    private final static String HOME = "index.jsp";
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            ProductDAO Productdao = new ProductDAO();
            CategoryDAO cateDao = new CategoryDAO();
            ProducerDAO producerDAO = new ProducerDAO();
            String action = request.getParameter("action");
            float max=Productdao.getMaxPrice();
            if (action == null) {
                action = "";
            }
            if (action.equals("search")) {
                request.setAttribute("Show_home", "search");
                String search = request.getParameter("search-name");
                request.setAttribute("name", search);
                String index = request.getParameter("index");
                if (index == null) {
                    index = "1";
                }
                int countPage = Productdao.countProductSearchByName(search);
                List<Product> listProduct = Productdao.get12ProductBySearch(search, Integer.parseInt(index));
                request.setAttribute("RESULT_PRODUCT", listProduct);
                request.setAttribute("RESULT_PRODUCT_LENGTH", countPage);
                request.setAttribute("INDEX",index);
                url = HOME;
            } else {
                String category = request.getParameter("category");
                if (category == null) {
                    List<Producer> listProducer = producerDAO.getListProducer("");
                    List<Category> listCate = cateDao.getCategory("");          
                    String indexPage = request.getParameter("indexPage");
                    if (indexPage == null) {
                        indexPage = "1";
                    }
                    List<Product> listProduct = Productdao.get12ProductByPage(Integer.parseInt(indexPage));
                    request.setAttribute("RESULT_PRODUCT", listProduct);
                    request.setAttribute("RESULT_CATEGOGY", listCate);
                    request.setAttribute("RESULT_Producer", listProducer);
                    request.setAttribute("CategoryID",category);
                    request.setAttribute("INDEX",indexPage);
                    url=SUCCESS;
                } else if (category != null) {
                    String index = request.getParameter("indexPage");
                    if (index == null) {
                        index = "1";
                    }
                    int countPage = Productdao.countProductSearch(category);
                    List<Producer> listProducer = producerDAO.getListProducer("");
                    List<Product> listProduct = Productdao.searchProductByCategory(Integer.parseInt(category), Integer.parseInt(index));
                    
                    if (listProduct.isEmpty()) {
                        request.setAttribute("NOTFOUND", "No products were found");
                    }
                    List<Category> listCate = cateDao.getCategory("");
                    request.setAttribute("tag", Integer.parseInt(category));
                    request.setAttribute("RESULT_PRODUCT", listProduct);
                    request.setAttribute("RESULT_CATEGOGY", listCate);
                    request.setAttribute("RESULT_Producer", listProducer);
                    request.setAttribute("RESULT_PRODUCT_LENGTH", countPage);
                    request.setAttribute("CategoryID",category);
                    request.setAttribute("PageCategory", "true");
                    request.setAttribute("MAX_PRICE", max);
                    request.setAttribute("INDEX",index);
                    url = SUCCESS;
                }
                
            }
            
        } catch (Exception e) {
        }
        
        request.getRequestDispatcher(url).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
