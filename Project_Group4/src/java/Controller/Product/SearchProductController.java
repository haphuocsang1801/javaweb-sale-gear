/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.Product;

import DAO.CategoryDAO;
import DAO.ProducerDAO;
import DAO.ProductDAO;
import entity.Category;
import entity.Producer;
import entity.Product;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author admin
 */
@WebServlet(name = "SearchProductController", urlPatterns = {"/SearchProductController"})
public class SearchProductController extends HttpServlet {
    
    private final static String ERROR="admin.jsp";
    private final static String SUCCESS="admin.jsp";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url=ERROR;
        try {
            request.setAttribute("Show_manage", "product");        
            String search=request.getParameter("name");
            ProducerDAO daoProducer=new ProducerDAO();
            CategoryDAO daoCategory= new CategoryDAO();
            List<Category> listCate= daoCategory.getCategory("");
            List<Producer> listProducer= daoProducer.getListProducer("");
            
            if(search==null || search.isEmpty()){
                search="";
            }
            ProductDAO dao=new ProductDAO();
            List<Product> list=dao.searchProduct(search);
            if(list!=null||list.size()>0){
                request.setAttribute("LIST_PRODUCER", listProducer);
                request.setAttribute("LIST_CATEGORY", listCate);
                request.setAttribute("RESULT_SEARCH_PRODUCT", list);
                url=SUCCESS;
            }
            else{
                request.setAttribute("ERROR_SEARCH_PRODUCT", "Product is not available");
                url=ERROR;
            }
        } catch (Exception e) {
        }
        finally{
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
