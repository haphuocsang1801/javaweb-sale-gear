/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.CategoryDAO;
import DAO.ProducerDAO;
import entity.Category;
import entity.Producer;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author admin
 */
@WebServlet(name = "ShowInputController", urlPatterns = {"/ShowInputController"})
public class ShowInputController extends HttpServlet {
    private final static String ERROR="admin.jsp";
    private final static String SUCCES="admin.jsp";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url=ERROR;
        try {
            String action=request.getParameter("action");
            if("ShowinsertCategory".equals(action)){ 
                request.setAttribute("INPUT_INSERT", action);
                url=SUCCES;
            }
            else if("ShowinsertProduct".equals(action)){
                CategoryDAO dao = new CategoryDAO();
                ProducerDAO daoproducer=new ProducerDAO();
                List<Producer> list_Producers = daoproducer.getListProducer("");
                List<Category> list = dao.getCategory(""); 
                request.setAttribute("INPUT_INSERT", action);
                request.setAttribute("LIST_CATEGORY", list);
                request.setAttribute("LIST_Producers", list_Producers);
                url=SUCCES;
            }
            else if("ShowInsertProducer".equals(action)){
                request.setAttribute("INPUT_INSERT", action);
            }
            
            
        } catch (Exception e) {
        }finally{
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
