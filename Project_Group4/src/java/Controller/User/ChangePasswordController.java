/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.User;

import DAO.UserDAO;
import entity.PasswordError;
import entity.User;
import entity.UserError;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author sanghpce150201
 */
@WebServlet(name = "ChangePasswordController", urlPatterns = {"/ChangePasswordController"})
public class ChangePasswordController extends HttpServlet {

    private static final String ERROR = "changePassword.jsp";
    private static final String SUCCESS = "changePassword.jsp";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = SUCCESS;
        try {
            String current = request.getParameter("current");
            String password = request.getParameter("password");
            String re_password = request.getParameter("re-password");
            
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute("LOGIN_USER");
            int userID = user.getUserID();
            UserDAO dao = new UserDAO();
            boolean checkPassword = dao.checkPassword(current, userID);
            PasswordError error = new PasswordError();
            boolean validPassword = true;
            if (!checkPassword) {
                validPassword = false;
                error.setCurrent_password("Enter a valid password and try again.");
            } 
            if (!password.equals(re_password)) {
                validPassword = false;
                error.setNew_password("Passwords do not match.");
            }
            UserError userError =new UserError();
            if(password.length()<5 ||password.length()>30){
                userError.setPasswordError("Password length must be 5 to 30 characters");
                validPassword=false;
            }
            if(validPassword){
                boolean check = dao.changePassword(password,userID);
                request.setAttribute("CHANGE_SUCCESS", "Password has been changed");
            }
            else{
                request.setAttribute("error", error);
                request.setAttribute("userError", userError);
            }
            

        } catch (Exception e) {
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
