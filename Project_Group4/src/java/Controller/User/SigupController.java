/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.User;

import DAO.UserDAO;
import entity.User;
import entity.UserError;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author admin
 */
@WebServlet(name = "SigupController", urlPatterns = {"/SigupController"})
public class SigupController extends HttpServlet {
    private final static String ERROR="signup.jsp";
    private final static String SUCCESS="login.jsp";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url=ERROR;
        String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        
        try {
            String username=request.getParameter("username");
            String fullname=request.getParameter("fullname");
            String email=request.getParameter("email");
            String phone=request.getParameter("numberphone");
            String password=request.getParameter("password");
            String address=request.getParameter("address");
                       
            boolean check=true;
            UserError userError =new UserError();
            if(username.length()<4 || username.length()>20){
                userError.setUserNameError("Username length must be 4-20 charaters");
                check=false;
            }
            if(fullname.length()<5 || fullname.length()>25){
                userError.setFullnameError("Full name length must be 5-25 charaters");
                check=false;
            }
            if(!email.matches(EMAIL_REGEX)){
                userError.setEmailError("Email is not validating");
                check=false;
            }
            if(phone.length()<10 || phone.length()>11){
                userError.setPhoneError("Phone number must be 10 to 11 numbers");
                check=false;
            }
            if(address.length()<5 ||address.length()>30){
                userError.setAddressError("Address length musr be 5 to 30 characters");
                check=false;
            }
            if(password.length()<5 ||password.length()>30){
                userError.setPasswordError("Password length musr be 5 to 30 characters");
                check=false;
            }
            if(check){
                UserDAO dao= new UserDAO();
                User user=new User(0, username, password, fullname, phone, address, email,"");
                request.setAttribute("USER_INPUT", user);
                boolean checkDuplicate=dao.checkDuplicateLogin(username);
                if(checkDuplicate){
                    userError.setUserNameError("Username already exists");
                    request.setAttribute("USER_SIGUP", userError);
                }
                else{
                    boolean inserUser=dao.insertUser(user);
                    if(inserUser){
                        url=SUCCESS;
                    }
                    else{
                        userError.setMessageError("There was an error during registration");
                        request.setAttribute("USER_SIGUP", userError);
                    }
                }
            }
            else{
                request.setAttribute("USER_SIGUP", userError);;
            }
            
            
        } catch (Exception e) {
            log("Error at SigUPControlller"+e.toString());
        }finally{
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
