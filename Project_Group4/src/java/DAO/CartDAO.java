/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author sanghpce150201
 */
public class CartDAO {

    Connection conn = null;
    PreparedStatement pr = null;
    ResultSet rs = null;

    public boolean insertCart(int userID, int quantity, float price, String productID) throws SQLException {
        boolean check = false;
        try {
            String query = "INSERT dbo.order_item\n"
                    + "(\n"
                    + "    quantity,\n"
                    + "    user_id,\n"
                    + "    price,\n"
                    + "    pro_id\n"
                    + ")\n"
                    + "VALUES\n"
                    + "(   ?,   -- quantity - int\n"
                    + "    ?,   -- user_id - int\n"
                    + "    ?, -- price - float\n"
                    + "    ?  -- pro_id - nvarchar(50)\n"
                    + "    )";
            conn = new DB_Connect.DBContext().getConnection();
            pr = conn.prepareStatement(query);
            pr.setInt(1, quantity);
            pr.setInt(2, userID);
            pr.setFloat(3, price);
            pr.setString(4, productID);
            check = pr.executeUpdate() > 0;
        } catch (Exception e) {
        } finally {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
        }
        return check;
    }

    public boolean minusQuantity(int quantity, String product_id) throws SQLException {
        boolean check = false;
        try {
            String query = "UPDATE dbo.product_detail SET quantity=quantity-? WHERE pro_id=?";
            conn = new DB_Connect.DBContext().getConnection();
            pr = conn.prepareStatement(query);
            pr.setInt(1, quantity);
            pr.setString(2, product_id);
            check = pr.executeUpdate() > 0;
        } catch (Exception e) {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
        }
        return check;
    }

    public int getQuantityProduct(String id) throws SQLException {
        int check = 0;
        try {
            String query = "SELECT quantity\n"
                    + "FROM dbo.product_detail\n"
                    + "WHERE pro_id=?";
            conn = new DB_Connect.DBContext().getConnection();
            pr = conn.prepareStatement(query);
            pr.setString(1, id);
            rs = pr.executeQuery();
            if (rs.next()) {
                check = rs.getInt(1);
            }
        } catch (Exception e) {
        } finally {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return check;
    }

    public boolean checkExits(String id) throws SQLException {
        boolean check = false;
        try {
            String query = "SELECT *\n"
                    + "FROM dbo.sold_product\n"
                    + "WHERE pro_id=?";
            conn = new DB_Connect.DBContext().getConnection();
            pr = conn.prepareStatement(query);
            pr.setString(1, id);
            rs = pr.executeQuery();
            if (rs.next()) {
                check = true;
            }
        } catch (Exception e) {
        } finally {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return check;
    }

    public void updateSoldProduct(String id, int quantity) throws SQLException {
        try {
            String query = "UPDATE dbo.sold_product SET sold_quantity=sold_quantity + ?\n"
                    + "WHERE pro_id=?";
            conn = new DB_Connect.DBContext().getConnection();
            pr = conn.prepareStatement(query);
            pr.setString(2, id);
            pr.setInt(1, quantity);
            pr.executeUpdate();
        } catch (Exception e) {
        } finally {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public void insertSoldProduct(String id, int quantity) throws SQLException {
        try {
            String query = "INSERT dbo.sold_product\n"
                    + "(\n"
                    + "    pro_id,\n"
                    + "    sold_quantity\n"
                    + ")\n"
                    + "VALUES\n"
                    + "(   ?, -- pro_id - nvarchar(50)\n"
                    + "    ?    -- sold_quantity - int\n"
                    + "    )";
            conn = new DB_Connect.DBContext().getConnection();
            pr = conn.prepareStatement(query);
            pr.setString(1, id);
            pr.setInt(2, quantity);
            pr.executeUpdate();
        } catch (Exception e) {
        } finally {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

}
