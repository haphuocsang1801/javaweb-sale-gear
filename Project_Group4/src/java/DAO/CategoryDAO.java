/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DB_Connect.DBContext;
import entity.Category;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author admin
 */
public class CategoryDAO {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public List<Category> getCategory(String name) throws SQLException {
        List<Category> list = new ArrayList<>();

        try {
            conn = new DB_Connect.DBContext().getConnection();
            String query = "SELECT *\n"
                    + "FROM dbo.product_type\n"
                    + "WHERE ptyle_id LIKE ? OR ptyle_name LIKE ?";
            ps=conn.prepareStatement(query);
            ps.setString(1,"%"+name+"%");
            ps.setString(2, "%"+name+"%");
            rs=ps.executeQuery();
            while (rs.next()) {                
                list.add(new Category(rs.getInt("ptyle_id"), rs.getString("ptyle_name")));
            }
        } catch (Exception e) {
        } finally {
            if (conn != null) {
                conn.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return list;
    }
    public boolean UpdateCategory(Category cate) throws SQLException {
        boolean check = false;
        try {
            String query = "update product_type\n"
                    + "set ptyle_name=?\n"
                    + "where ptyle_id=?";
            conn = new DB_Connect.DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, cate.getName());
            ps.setInt(2, cate.getId());
            check = ps.executeUpdate() > 0;

        } catch (Exception e) {
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return check;
    }

    public boolean deleteCategory(int id) throws SQLException {
        boolean check = false;
        try {
            String query = "delete from product_type \n"
                    + "where[ptyle_id]= ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setInt(1, id);
            check = ps.executeUpdate() > 0;
        } catch (Exception e) {
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return check;
    }

    public boolean insertCategory(Category cate) throws SQLException {
        boolean check = false;
        try {
            conn = new DBContext().getConnection();
            String query = "INSERT INTO dbo.product_type\n"
                    + "(\n"
                    + "    ptyle_id,\n"
                    + "    ptyle_name\n"
                    + ")\n"
                    + "VALUES\n"
                    + "(?,?)";
            ps = conn.prepareStatement(query);
            ps.setInt(1, cate.getId());
            ps.setString(2, cate.getName());
            check = ps.executeUpdate() > 0;
        } catch (Exception e) {
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return check;
    }

    public boolean checkDuplicateCategory(Category cate) throws SQLException {
        boolean check = false;
        try {
            conn = new DBContext().getConnection();
            String query = "SELECT *\n"
                    + "FROM dbo.product_type\n"
                    + "WHERE ptyle_name=?";
            ps = conn.prepareStatement(query);
            ps.setString(1, cate.getName());
            rs = ps.executeQuery();
            if (rs.next()) {
                check = true;
            }
        } catch (Exception e) {
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return check;
    }
}
