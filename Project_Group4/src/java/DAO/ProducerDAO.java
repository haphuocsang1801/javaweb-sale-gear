/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import entity.Producer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author admin
 */
public class ProducerDAO {

    Connection conn = null;
    PreparedStatement pr = null;
    ResultSet rs = null;

    public List<Producer> getListProducer(String name) throws SQLException {
        List<Producer> list = new ArrayList<>();
        try {
            String query = "SELECT *\n"
                    + "FROM dbo.NSX\n"
                    + "WHERE nsx_name LIKE ?";
            conn = new DB_Connect.DBContext().getConnection();
            pr = conn.prepareStatement(query);
            pr.setString(1, "%" + name + "%");
            rs = pr.executeQuery();
            while (rs.next()) {
                list.add(new Producer(rs.getString("nsx_id"), rs.getString("nsx_name")));
            }
        } catch (Exception e) {
        } finally {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return list;
    }

    public boolean updateProducer(String id, String name) throws SQLException {
        boolean check = false;
        try {
            String query = "UPDATE dbo.NSX SET nsx_name=?\n"
                    + "WHERE nsx_id=?";
            conn = new DB_Connect.DBContext().getConnection();
            pr = conn.prepareStatement(query);
            pr.setString(1, name);
            pr.setString(2, id);
            check = pr.executeUpdate() > 0;
        } catch (Exception e) {
        } finally {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
        }
        return check;
    }

    public boolean deleteProducer(String id) throws SQLException {
        boolean check = false;
        try {
            String query = "DELETE dbo.NSX\n"
                    + "WHERE nsx_id=?";
            conn = new DB_Connect.DBContext().getConnection();
            pr = conn.prepareStatement(query);
            pr.setString(1, id);
            check = pr.executeUpdate() > 0;
        } catch (Exception e) {
        } finally {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
        }
        return check;
    }

    public boolean insertProducer(String id, String name) throws SQLException {
        boolean check = false;
        try {
            String query = "INSERT dbo.NSX\n"
                    + "(\n"
                    + "    nsx_id,\n"
                    + "    nsx_name\n"
                    + ")\n"
                    + "VALUES\n"
                    + "(   ?, -- nsx_id - nchar(10)\n"
                    + "    ?  -- nsx_name - nvarchar(50)\n"
                    + "    )";
            conn = new DB_Connect.DBContext().getConnection();
            pr = conn.prepareStatement(query);
            pr.setString(1, id);
            pr.setString(2, name);
            check = pr.executeUpdate() > 0;
        } catch (Exception e) {
        } finally {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
        }
        return check;
    }

    public boolean checkDuplicate(String id) throws SQLException {
        boolean check = false;
        try {
            String query = "SELECT *\n"
                    + "FROM dbo.NSX\n"
                    + "WHERE nsx_id=?";
            conn = new DB_Connect.DBContext().getConnection();
            pr = conn.prepareStatement(query);
            pr.setString(1, id);
            rs=pr.executeQuery();
            if(rs.next()){
                check=true;
            }

        } catch (Exception e) {
        } finally {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
        }
        return check;
    }
}
