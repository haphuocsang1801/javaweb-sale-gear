/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import entity.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author admin
 */
public class ProductDAO {

    Connection conn = null;
    PreparedStatement pr = null;
    ResultSet rs = null;

    public List<Product> searchProduct(String search) throws SQLException {
        List<Product> list = new ArrayList<>();
        try {
            conn = new DB_Connect.DBContext().getConnection();
            String query = "select p.pro_id, p.pro_name, t.ptyle_name, p.quantity, p.[description], p.price, nsx.nsx_name, p.[image], p.date\n"
                    + "  from dbo.product_detail p\n"
                    + "  inner join dbo.product_type t on t.ptyle_id = p.ptyle_id\n"
                    + "  inner join dbo.NSX nsx on nsx.nsx_id = p.nsx_id\n"
                    + "  where p.pro_name like ?";
            pr = conn.prepareStatement(query);
            pr.setString(1, "%" + search + "%");
            rs = pr.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getString("pro_id"),
                        rs.getString("pro_name"),
                        rs.getString("ptyle_name"),
                        rs.getFloat("price"),
                        rs.getString("description"),
                        rs.getString("nsx_name"),
                        rs.getString("image"),
                        rs.getInt("quantity"),
                        rs.getDate("date")));

            }

        } catch (Exception e) {
        } finally {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return list;
    }

    public Product searchProductByID(String search) throws SQLException {
        Product obj = new Product();
        try {
            conn = new DB_Connect.DBContext().getConnection();
            String query = "select p.pro_id, p.pro_name, t.ptyle_name, p.quantity, p.[description], p.price, nsx.nsx_name, p.[image], p.date\n"
                    + "  from dbo.product_detail p\n"
                    + "  inner join dbo.product_type t on t.ptyle_id = p.ptyle_id\n"
                    + "  inner join dbo.NSX nsx on nsx.nsx_id = p.nsx_id\n"
                    + "  where p.pro_id like ?";
            pr = conn.prepareStatement(query);
            pr.setString(1, "%" + search + "%");
            rs = pr.executeQuery();
            while (rs.next()) {
                obj = (new Product(rs.getString("pro_id"),
                        rs.getString("pro_name"),
                        rs.getString("ptyle_name"),
                        rs.getFloat("price"),
                        rs.getString("description"),
                        rs.getString("nsx_name"),
                        rs.getString("image"),
                        rs.getInt("quantity"),
                        rs.getDate("date")));

            }

        } catch (Exception e) {
        } finally {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return obj;
    }

    public List<Product> searchProductByCategory(int cateogry, int index) throws SQLException {
        List<Product> list = new ArrayList<>();
        try {
            conn = new DB_Connect.DBContext().getConnection();
            String query = "select p.pro_id, p.pro_name, t.ptyle_name, p.quantity, p.[description], p.price, nsx.nsx_name, p.[image], p.date\n"
                    + "from dbo.product_detail p\n"
                    + "inner join dbo.product_type t on t.ptyle_id = p.ptyle_id\n"
                    + "inner join dbo.NSX nsx on nsx.nsx_id = p.nsx_id\n"
                    + "where t.ptyle_id like ? AND p.[date] <= getdate()\n"
                    + "ORDER BY p.date DESC\n"
                    + "OFFSET ? ROWS\n"
                    + "FETCH FIRST 9 ROWS ONLY";
            pr = conn.prepareStatement(query);
            pr.setString(1, "%" + cateogry + "%");
            pr.setInt(2, (index - 1) * 9);
            rs = pr.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getString("pro_id"),
                        rs.getString("pro_name"),
                        rs.getString("ptyle_name"),
                        rs.getFloat("price"),
                        rs.getString("description"),
                        rs.getString("nsx_name"),
                        rs.getString("image"),
                        rs.getInt("quantity"),
                        rs.getDate("date")));

            }

        } catch (Exception e) {
        } finally {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return list;
    }

    public List<Product> getTop5Product() throws SQLException {
        List<Product> list = new ArrayList<>();
        try {
            conn = new DB_Connect.DBContext().getConnection();
            String query = "select top(5) pro_id, pro_name, ptyle_id, price, [description], nsx_id, [image], quantity, [date]\n"
                    + "from dbo.product_detail p\n"
                    + "where p.[date] < getdate()\n"
                    + "order by p.[date] desc;";
            pr = conn.prepareStatement(query);
            rs = pr.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getString("pro_id"),
                        rs.getString("pro_name"),
                        rs.getString("ptyle_id"),
                        rs.getFloat("price"),
                        rs.getString("description"),
                        rs.getString("nsx_id"),
                        rs.getString("image"),
                        rs.getInt("quantity"),
                        rs.getDate("date")));
            }

        } catch (Exception e) {
        } finally {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return list;
    }

    public boolean updateProduct(String id, String name, float price, int quantity, String description, int typeid, String nsxID) throws SQLException {
        boolean check = false;
        try {
            conn = new DB_Connect.DBContext().getConnection();
            String query = "UPDATE dbo.product_detail\n"
                    + "SET pro_name=?,price=?,description=?,quantity=?,ptyle_id=?,nsx_id=?\n"
                    + "WHERE pro_id=?";
            pr = conn.prepareStatement(query);
            pr.setString(1, name);
            pr.setFloat(2, price);
            pr.setString(3, description);
            pr.setInt(4, quantity);
            pr.setInt(5, typeid);
            pr.setString(6, nsxID);
            pr.setString(7, id);
            check = pr.executeUpdate() > 0;
        } catch (Exception e) {
        } finally {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
        }
        return check;
    }

    public boolean DeleteProduct(String id) throws SQLException {
        boolean check = false;
        try {
            conn = new DB_Connect.DBContext().getConnection();
            String query = "DELETE FROM dbo.product_detail \n"
                    + "WHERE pro_id=?";
            pr = conn.prepareStatement(query);
            pr.setString(1, id);
            check = pr.executeUpdate() > 0;
        } catch (Exception e) {
        } finally {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
        }
        return check;

    }

    public boolean InsertProduct(Product obj) throws SQLException {
        boolean check = false;
        try {
            conn = new DB_Connect.DBContext().getConnection();
            String query = "INSERT dbo.product_detail\n"
                    + "(\n"
                    + "    pro_id,\n"
                    + "    pro_name,\n"
                    + "    ptyle_id,\n"
                    + "    price,\n"
                    + "    description,\n"
                    + "    nsx_id,\n"
                    + "    image,\n"
                    + "    quantity,\n"
                    + "    date\n"
                    + ")\n"
                    + "VALUES\n"
                    + "(   ?,      -- pro_id - nchar(10)\n"
                    + "    ?,      -- pro_name - nvarchar(50)\n"
                    + "    ?,      -- ptyle_id - nchar(10)\n"
                    + "    ?,      -- price - float\n"
                    + "    ?,      -- description - nvarchar(max)\n"
                    + "    ?,      -- nsx_id - nchar(10)\n"
                    + "    ?,      -- image - nvarchar(max)\n"
                    + "    ?,        -- quantity - int\n"
                    + "    GETDATE() -- date - date\n"
                    + "    )";
            pr = conn.prepareStatement(query);
            pr.setString(1, obj.getId());
            pr.setString(2, obj.getName());
            pr.setString(3, obj.getType_id());
            pr.setFloat(4, obj.getPrice());
            pr.setString(5, obj.getDesc());
            pr.setString(6, obj.getNsx_name());
            pr.setString(7, obj.getImage());
            pr.setInt(8, obj.getQuantity());
            check = pr.executeUpdate() > 0;

        } catch (Exception e) {
        } finally {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
        }
        return check;
    }

    public Product getProduct(String id) throws SQLException {
        Product obj = new Product();
        try {
            String query = "select p.pro_id, p.pro_name, t.ptyle_name, p.quantity, p.[description], p.price, nsx.nsx_name, p.[image], p.date\n"
                    + "from dbo.product_detail p\n"
                    + "inner join dbo.product_type t on t.ptyle_id = p.ptyle_id\n"
                    + "inner join dbo.NSX nsx on nsx.nsx_id = p.nsx_id\n"
                    + "where p.pro_id=?";
            conn = new DB_Connect.DBContext().getConnection();
            pr = conn.prepareStatement(query);
            pr.setString(1, id);
            rs = pr.executeQuery();
            if (rs.next()) {
                obj = new Product(rs.getString("pro_id"), rs.getString("pro_name"), rs.getString("ptyle_name"), rs.getFloat("price"), rs.getString("description"), rs.getString("nsx_name"), rs.getString("image"), rs.getInt("quantity"), rs.getDate("date"));
            }
        } catch (Exception e) {
        } finally {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return obj;
    }

    public int getNumberPage() throws SQLException {
        String quey = "SELECT COUNT(*)\n"
                + "FROM dbo.product_detail";
        try {
            conn = new DB_Connect.DBContext().getConnection();
            pr = conn.prepareStatement(quey);
            rs = pr.executeQuery();
            if (rs.next()) {
                int total = rs.getInt(1);
                int countPage = 0;
                countPage = total / 12;
                if (total != 0) {
                    countPage++;
                }
                return countPage;
            }

        } catch (Exception e) {
        } finally {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return 0;
    }

    public List<Product> get12ProductByPage(int index) throws SQLException {
        List<Product> list = new ArrayList<>();
        try {
            String query = "select p.pro_id, p.pro_name, t.ptyle_name, p.quantity, p.[description], p.price, nsx.nsx_name, p.[image], p.date\n"
                    + "from dbo.product_detail p\n"
                    + "inner join dbo.product_type t on t.ptyle_id = p.ptyle_id\n"
                    + "inner join dbo.NSX nsx on nsx.nsx_id = p.nsx_id\n"
                    + "where p.[date] <= getdate()\n"
                    + "ORDER BY p.date DESC\n"
                    + "OFFSET ? ROWS\n"
                    + "FETCH FIRST 12 ROWS ONLY";
            conn = new DB_Connect.DBContext().getConnection();
            pr = conn.prepareStatement(query);
            pr.setInt(1, (index - 1) * 12);
            rs = pr.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getString("pro_id"), rs.getString("pro_name"), rs.getString("ptyle_name"), rs.getFloat("price"), rs.getString("description"), rs.getString("nsx_name"), rs.getString("image"), rs.getInt("quantity"), rs.getDate("date")));
            }

        } catch (Exception e) {
        } finally {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return list;
    }

    public int countProductSearch(String search) throws SQLException {
        int total = 0;
        try {
            conn = new DB_Connect.DBContext().getConnection();
            String query = "select COUNT(*)\n"
                    + " from dbo.product_detail p\n"
                    + " inner join dbo.product_type t on t.ptyle_id = p.ptyle_id\n"
                    + "inner join dbo.NSX nsx on nsx.nsx_id = p.nsx_id\n"
                    + " where t.ptyle_id=?";
            pr = conn.prepareStatement(query);
            pr.setString(1, search);
            rs = pr.executeQuery();
            if (rs.next()) {
                total = rs.getInt(1);
                int countPage = 0;
                countPage = total / 9;
                if (total % 9 != 0) {
                    countPage++;
                }
                return countPage;
            }

        } catch (Exception e) {
        } finally {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return 0;

    }

    public int countProductSearchByName(String search) throws SQLException {
        int total = 0;
        try {
            conn = new DB_Connect.DBContext().getConnection();
            String query = "select COUNT(*)\n"
                    + "from dbo.product_detail p\n"
                    + " inner join dbo.product_type t on t.ptyle_id = p.ptyle_id\n"
                    + "inner join dbo.NSX nsx on nsx.nsx_id = p.nsx_id\n"
                    + "where p.pro_name LIKE ?";
            pr = conn.prepareStatement(query);
            pr.setString(1, "%" + search + "%");
            rs = pr.executeQuery();
            if (rs.next()) {
                total = rs.getInt(1);
                int countPage = 0;
                countPage = total / 12;
                if (total % 12 != 0) {
                    countPage++;
                }
                return countPage;
            }

        } catch (Exception e) {
        } finally {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return 0;

    }

    public List<Product> get12ProductBySearch(String search, int index) throws SQLException {
        List<Product> list = new ArrayList<>();
        try {
            conn = new DB_Connect.DBContext().getConnection();
            String query = "select p.pro_id, p.pro_name, t.ptyle_name, p.quantity, p.[description], p.price, nsx.nsx_name, p.[image], p.date\n"
                    + " from dbo.product_detail p\n"
                    + " inner join dbo.product_type t on t.ptyle_id = p.ptyle_id\n"
                    + "inner join dbo.NSX nsx on nsx.nsx_id = p.nsx_id\n"
                    + " where p.pro_name like ? AND p.[date] <= getdate()\n"
                    + " ORDER BY p.date DESC\n"
                    + "OFFSET ? ROWS\n"
                    + "FETCH FIRST 12 ROWS ONLY";
            pr = conn.prepareStatement(query);
            pr.setString(1, "%" + search + "%");
            pr.setInt(2, (index - 1) * 12);
            rs = pr.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getString("pro_id"),
                        rs.getString("pro_name"),
                        rs.getString("ptyle_name"),
                        rs.getFloat("price"),
                        rs.getString("description"),
                        rs.getString("nsx_name"),
                        rs.getString("image"),
                        rs.getInt("quantity"),
                        rs.getDate("date")));

            }

        } catch (Exception e) {
        } finally {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return list;
    }

    public int countSearchProductByCategory(int cateogry) throws SQLException {
        try {
            conn = new DB_Connect.DBContext().getConnection();
            String query = "select count(*)"
                    + "from dbo.product_detail p\n"
                    + "inner join dbo.product_type t on t.ptyle_id = p.ptyle_id\n"
                    + "inner join dbo.NSX nsx on nsx.nsx_id = p.nsx_id\n"
                    + "where t.ptyle_id like ?";
            pr = conn.prepareStatement(query);
            pr.setString(1, "%" + cateogry + "%");
            rs = pr.executeQuery();
            if (rs.next()) {
                int total = rs.getInt(1);
                int countPage = 0;
                countPage = total / 12;
                if (total != 0) {
                    countPage++;
                }
                return countPage;
            }

        } catch (Exception e) {
        } finally {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return 0;
    }

    public int countProductFilter(String nsx_id, String category_id, float min, float max) throws SQLException {
        try {
            String query = "select COUNT(*)\n"
                    + "from dbo.product_detail p\n"
                    + "inner join dbo.product_type t on t.ptyle_id = p.ptyle_id\n"
                    + "inner join dbo.NSX nsx on nsx.nsx_id = p.nsx_id\n"
                    + "WHERE t.ptyle_id=? AND nsx.nsx_id=? AND price BETWEEN ? AND ?\n"
                    + " AND p.[date] <= getdate()";
            conn = new DB_Connect.DBContext().getConnection();
            pr = conn.prepareStatement(query);
            pr.setString(2, nsx_id);
            pr.setString(1, category_id);
            pr.setFloat(3, min);
            pr.setFloat(4, max);
            rs = pr.executeQuery();
            if (rs.next()) {
                int total = rs.getInt(1);
                int countPage = 0;
                countPage = total / 9;
                if (total % 9 != 0) {
                    countPage++;
                }
                return countPage;
            }

        } catch (Exception e) {
        } finally {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return 0;
    }

    public List<Product> get9ProductByFilter(int index, String nsx_id, String category_id, float min, float max) throws SQLException {
        List<Product> list = new ArrayList<>();
        try {
            String query = "select p.pro_id, p.pro_name, t.ptyle_name, p.quantity, p.[description], p.price, nsx.nsx_name, p.[image], p.date\n"
                    + "from dbo.product_detail p\n"
                    + "inner join dbo.product_type t on t.ptyle_id = p.ptyle_id\n"
                    + "inner join dbo.NSX nsx on nsx.nsx_id = p.nsx_id\n"
                    + "WHERE t.ptyle_id=? AND nsx.nsx_id=? AND price BETWEEN ? AND ?\n"
                    + " AND p.[date] <= getdate()\n"
                    + "ORDER BY p.date DESC\n"
                    + "OFFSET ? ROWS\n"
                    + "FETCH \n"
                    + "FIRST 9 ROWS ONLY";
            conn = new DB_Connect.DBContext().getConnection();
            pr = conn.prepareStatement(query);
            pr.setString(1, category_id);
            pr.setString(2, nsx_id);
            pr.setFloat(3, min);
            pr.setFloat(4, max);
            pr.setInt(5, (index - 1) * 9);
            rs = pr.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getString("pro_id"),
                        rs.getString("pro_name"),
                        rs.getString("ptyle_name"),
                        rs.getFloat("price"),
                        rs.getString("description"),
                        rs.getString("nsx_name"),
                        rs.getString("image"),
                        rs.getInt("quantity"),
                        rs.getDate("date")));
            }

        } catch (Exception e) {
        } finally {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return list;
    }

    public List<String> top5Best() throws SQLException {
        List<String> list = new ArrayList<>();
        try {
            String query = "SELECT TOP(4) pro_id,sold_quantity\n"
                    + "FROM dbo.sold_product\n"
                    + "ORDER BY sold_quantity DESC";
            conn = new DB_Connect.DBContext().getConnection();
            pr = conn.prepareStatement(query);
            rs = pr.executeQuery();
            while (rs.next()) {
                list.add(rs.getString(1));
            }
        } catch (Exception e) {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return list;
    }

    public float getMaxPrice() throws SQLException {
        float max = 0;
        try {
            String query = "select max(price)\n"
                    + "from product_detail";
            conn = new DB_Connect.DBContext().getConnection();
            pr = conn.prepareStatement(query);
            rs = pr.executeQuery();
            if (rs.next()) {
                max = rs.getFloat(1);
            }
        } catch (Exception e) {
            if (conn != null) {
                conn.close();
            }
            if (pr != null) {
                pr.close();
            }
        }
        return max;
    }

}
