package DAO;

import DB_Connect.DBContext;
import entity.CartItems;
import entity.Category;
import entity.History;
import entity.Product;
import entity.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDAO {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public User checkLogin(String username, String password) throws SQLException {
        User check = null;
        try {
            String query = "Select * from [user] where [user_name] = ? and [password] = ?";
            conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, username);
            ps.setString(2, password);
            rs = ps.executeQuery();

            if (rs.next()) {
                check = new User(rs.getInt("user_id"), rs.getString("user_name"), rs.getString("password"), rs.getString("full_name"), rs.getString("phone"), rs.getString("address"), rs.getString("email"), rs.getString("roleID"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return check;
    }

    public boolean insertUser(User user) throws SQLException {
        boolean check = false;
        try {
            conn = new DBContext().getConnection();
            String query = "INSERT INTO dbo.[user]\n"
                    + "(\n"
                    + "    user_name,\n"
                    + "    password,\n"
                    + "    full_name,\n"
                    + "    phone,\n"
                    + "    address,\n"
                    + "    email,\n"
                    + "    roleID\n"
                    + ")\n"
                    + "VALUES(?,?,?,?,?,?,?)";
            ps = conn.prepareStatement(query);
            ps.setString(1, user.getUserName());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getFullname());
            ps.setString(4, user.getPhone());
            ps.setString(5, user.getAddress());
            ps.setString(6, user.getEmail());
            ps.setString(7, "US");
            check = ps.executeUpdate() > 0;

        } catch (Exception e) {
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return check;
    }

    public boolean checkDuplicateLogin(String username) throws SQLException {
        boolean check = false;
        try {
            conn = new DB_Connect.DBContext().getConnection();
            String query = "SELECT *\n"
                    + "FROM dbo.[user]\n"
                    + "WHERE [user_name]=?";
            ps = conn.prepareStatement(query);
            ps.setString(1, username);
            rs = ps.executeQuery();
            if (rs.next()) {
                check = true;
            }
        } catch (Exception e) {
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }

        return check;
    }

    

    public boolean updateUser(String phone, String address, String email, int id) throws SQLException {
        boolean check = false;
        try {
            String query = "UPDATE dbo.[user] SET phone=?, address=?,email=?\n"
                    + "WHERE user_id=?";
            conn = new DB_Connect.DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setString(1, phone);
            ps.setString(2, address);
            ps.setString(3, email);
            ps.setInt(4, id);
            check = ps.executeUpdate() > 0;

        } catch (Exception e) {
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return check;
    }

    public boolean checkPassword(String password, int userID) throws SQLException {
        boolean check = false;
        try {
            String query = "SELECT *\n"
                    + "FROM dbo.[user]\n"
                    + "WHERE user_id=? AND password=?";
            conn = new DB_Connect.DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setInt(1, userID);
            ps.setString(2, password);
            rs = ps.executeQuery();
            if (rs.next()) {
                check = true;
            }

        } catch (Exception e) {
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return check;
    }

    public boolean changePassword(String password, int userID) throws SQLException {
        boolean check = false;
        try {
            String query = "UPDATE dbo.[user] SET password=?\n"
                    + "WHERE user_id=?";
            conn = new DB_Connect.DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setInt(2, userID);
            ps.setString(1, password);
            check = ps.executeUpdate() > 0;

        } catch (Exception e) {
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return check;
    }

    public List<History> getListHistory(int userID) throws SQLException {
        List<History> list = new ArrayList<>();
        try {
            String query = "SELECT pro.image, pro.pro_name,ord.quantity,ord.price,ord.date_of_payment\n"
                    + "FROM dbo.product_detail pro JOIN dbo.order_item ord\n"
                    + "ON pro.pro_id=ord.pro_id\n"
                    + "WHERE user_id=?";
            conn = new DB_Connect.DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setInt(1, userID);
            rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new History(userID, rs.getString(1), rs.getString(2), rs.getInt(3), rs.getFloat(4), rs.getDate(5)));
            }

        } catch (Exception e) {
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return list;
    }

    public boolean checkDuplicateCategoryByID(int id) throws SQLException {
        boolean check = false;
        try {
            conn = new DBContext().getConnection();
            String query = "SELECT *\n"
                    + "FROM dbo.product_type\n"
                    + "WHERE ptyle_id=?";
            ps = conn.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                check = true;
            }
        } catch (Exception e) {
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return check;
    }
}
