/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author sanghpce150201
 */
public class Cart {

    private Map<String, CartItems> cart;

    public Cart() {
    }

    public Cart(Map<String, CartItems> cart) {
        this.cart = cart;
    }

    public Map<String, CartItems> getCart() {
        return cart;
    }

    public void setCart(Map<String, CartItems> cart) {
        this.cart = cart;
    }

    public void add(CartItems cartItems) {
        if (this.cart == null) {
            this.cart = new HashMap<>();
        }
        if (this.cart.containsKey(cartItems.getProduct().getId().trim())) {
            int currentQuantity = this.cart.get(cartItems.getProduct().getId().trim()).getQuanity_cart();
            cartItems.setQuanity_cart(currentQuantity + cartItems.getQuanity_cart());
        }
        cart.put(cartItems.getProduct().getId().trim(), cartItems);
    }

    public void delete(String id) {
        if (this.cart == null) {
            return;
        }
        if (this.cart.containsKey(id.trim())) {
            this.cart.remove(id.trim());
        }
    }

    public void update(String id, CartItems newProduct) {
        if (this.cart == null) {
            return;
        }
        if (this.cart.containsKey(id.trim())) {
            this.cart.replace(id, newProduct);
        }
    }

}
