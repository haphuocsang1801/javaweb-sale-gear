/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.Date;

/**
 *
 * @author sanghpce150201
 */
public class CartItems {
    private Product product;
    private int quanity_cart;
    private float price_cart;
    private int userID;
    private Date date;

    public CartItems() {
    }

    public CartItems(Product product, int quanity_cart, float price_cart, int userID,Date date) {
        this.product = product;
        this.quanity_cart = quanity_cart;
        this.price_cart = price_cart;
        this.userID = userID;
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuanity_cart() {
        return quanity_cart;
    }

    public void setQuanity_cart(int quanity_cart) {
        this.quanity_cart = quanity_cart;
    }

    public float getPrice_cart() {
        return price_cart;
    }

    public void setPrice_cart(float price_cart) {
        this.price_cart = price_cart;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }
    
}
