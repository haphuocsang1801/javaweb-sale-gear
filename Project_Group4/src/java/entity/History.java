/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.Date;

/**
 *
 * @author sanghpce150201
 */
public class History {

    private int userID;
    private String image;
    private String name;
    private int quanity_cart;
    private float total_price;
    private Date date;

    public History(int userID, String image, String name, int quanity_cart, float total_price, Date date) {
        this.userID = userID;
        this.image = image;
        this.name = name;
        this.quanity_cart = quanity_cart;
        this.total_price = total_price;
        this.date = date;
    }

    public History() {
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuanity_cart() {
        return quanity_cart;
    }

    public void setQuanity_cart(int quanity_cart) {
        this.quanity_cart = quanity_cart;
    }

    public float getTotal_price() {
        return total_price;
    }

    public void setTotal_price(float total_price) {
        this.total_price = total_price;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
