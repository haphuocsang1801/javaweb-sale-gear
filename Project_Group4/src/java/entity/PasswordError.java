/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author sanghpce150201
 */
public class PasswordError {
    private String current_password;
    private String new_password;
    private String re_password;

    public PasswordError() {
    }

    public PasswordError(String current_password, String new_password, String re_password) {
        this.current_password = current_password;
        this.new_password = new_password;
        this.re_password = re_password;
    }

    public String getCurrent_password() {
        return current_password;
    }

    public void setCurrent_password(String current_password) {
        this.current_password = current_password;
    }

    public String getNew_password() {
        return new_password;
    }

    public void setNew_password(String new_password) {
        this.new_password = new_password;
    }

    public String getRe_password() {
        return re_password;
    }

    public void setRe_password(String re_password) {
        this.re_password = re_password;
    }
    
}
