/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author admin
 */
public class Producer {
    private String nsx_id;
    private String nsx_name;

    public Producer(String nsx_id, String nsx_name) {
        this.nsx_id = nsx_id;
        this.nsx_name = nsx_name;
    }

    public Producer() {
    }

    public String getNsx_id() {
        return nsx_id;
    }

    public void setNsx_id(String nsx_id) {
        this.nsx_id = nsx_id;
    }

    public String getNsx_name() {
        return nsx_name;
    }

    public void setNsx_name(String nsx_name) {
        this.nsx_name = nsx_name;
    }
    
}
