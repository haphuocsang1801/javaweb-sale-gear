/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.Date;

/**
 *
 * @author admin
 */
public class Product {
    private String id;             
    private String name;
    private String type_id;
    private float price;
    private String desc;
    private String nsx_name;
    private String image;
    private int quantity;
    private Date date;

    public Product() {
    }

    public Product(String id, String name, String type_id, float price, String desc, String nsx_name, String image, int quantity, Date date) {
        this.id = id;
        this.name = name;
        this.type_id = type_id;
        this.price = price;
        this.desc = desc;
        this.nsx_name = nsx_name;
        this.image = image;
        this.quantity = quantity;
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType_id() {
        return type_id;
    }

    public void setType_id(String type_id) {
        this.type_id = type_id;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getNsx_name() {
        return nsx_name;
    }

    public void setNsx_name(String nsx_name) {
        this.nsx_name = nsx_name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    
    
}
