/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author admin
 */
public class UserError {
    private String userIDError;
    private String userNameError;
    private String passwordError;
    private String fullnameError;
    private String phoneError;
    private String addressError;
    private String emailError;
    private String roleIDError;
    private String messageError;
    
    public UserError(String userIDError, String userNameError, String passwordError, String fullnameError, String phoneError, String addressError, String emailError, String roleIDError,String messageError) {
        this.userIDError = userIDError;
        this.userNameError = userNameError;
        this.passwordError = passwordError;
        this.fullnameError = fullnameError;
        this.phoneError = phoneError;
        this.addressError = addressError;
        this.emailError = emailError;
        this.roleIDError = roleIDError;
        this.messageError= messageError;
    }

    public UserError() {
        this.userIDError = "";
        this.userNameError = "";
        this.passwordError = "";
        this.fullnameError = "";
        this.phoneError = "";
        this.addressError = "";
        this.emailError = "";
        this.roleIDError = "";
        this.messageError="";
    }

    public String getMessageError() {
        return messageError;
    }

    public void setMessageError(String messageError) {
        this.messageError = messageError;
    }

    public String getUserIDError() {
        return userIDError;
    }

    public void setUserIDError(String userIDError) {
        this.userIDError = userIDError;
    }

    public String getUserNameError() {
        return userNameError;
    }

    public void setUserNameError(String userNameError) {
        this.userNameError = userNameError;
    }

    public String getPasswordError() {
        return passwordError;
    }

    public void setPasswordError(String passwordError) {
        this.passwordError = passwordError;
    }

    public String getFullnameError() {
        return fullnameError;
    }

    public void setFullnameError(String fullnameError) {
        this.fullnameError = fullnameError;
    }

    public String getPhoneError() {
        return phoneError;
    }

    public void setPhoneError(String phoneError) {
        this.phoneError = phoneError;
    }

    public String getAddressError() {
        return addressError;
    }

    public void setAddressError(String addressError) {
        this.addressError = addressError;
    }

    public String getEmailError() {
        return emailError;
    }

    public void setEmailError(String emailError) {
        this.emailError = emailError;
    }

    public String getRoleIDError() {
        return roleIDError;
    }

    public void setRoleIDError(String roleIDError) {
        this.roleIDError = roleIDError;
    }
    
}
