<%-- 
    Document   : orderHistory
    Created on : Nov 5, 2021, 9:50:48 PM
    Author     : sanghpce150201
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link
            href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;700&display=swap"
            rel="stylesheet"
            />
        <script
            src="https://kit.fontawesome.com/85150e2613.js"
            crossorigin="anonymous"
        ></script>
        <script
            type="module"
            src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"
        ></script>
        <script
            nomodule
            src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"
        ></script>

        <link href="css/reset.css" rel="stylesheet" type="text/css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
    <body>
        <div class="wrapper">
            <section class="header">
                <jsp:include page="header.jsp"/> 
            </section>
            <section class="purchar">
                <div class="purchar-title">
                    Your purchase history
                </div>
                <c:if test="${not empty requestScope.NOTFOUND}">
                    <p>You have not purchased any products yet</p>
                </c:if>
                <c:if test="${ empty requestScope.NOTFOUND}">
                    <div class="purchar-container">
                    <table border="1" id="table">
                        <thead>
                            <tr>
                                <th>Product</th>
                                <th>Name</th>
                                <th>Quantity</th>
                                <th>Total Price</th>
                                <th>Purchase date</th>
                            </tr>
                        </thead>
                        <tbody>

                            <c:forEach var="o" items="${requestScope.LIST_HISTORY}">
                                <tr>
                                    <td>
                                        <img class="product-image-input" src="${o.image}" alt="">
                                    </td>
                                    <td>${o.name}</td>
                                    <td>${o.quanity_cart}</td>
                                    <td>${o.total_price}</td>
                                    <td>${o.date}</td>
                                </tr>
                            </c:forEach>

                        </tbody>
                    </table>
                </div>
                </c:if>
            </section>
            <section class="footer">
                <jsp:include page="footer.jsp"/> 
            </section>
    </body>
</body>
</html>
