<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/reset.css" rel="stylesheet" type="text/css"/>
        <script
            type="module"
            src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"
        ></script>
        <script
            nomodule
            src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"
        ></script>
        <link
        rel="stylesheet"
        type="text/css"
        href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"
      />
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
       
    </head>
    <body>
        <div class="image-slider">
             <c:forEach items="${requestScope.RESULT_SEARCH_PRODUCT_TOP5}" var="o">
                <div class="image-item">
                    <a href="detail?action=${o.id}" class="image">
                        <img src="${o.image}" class="slide-img" >
                    </a>
                </div>
            </c:forEach> 
        </div>

        <script
          type="text/javascript"
          src="https://code.jquery.com/jquery-1.11.0.min.js"
        ></script>
        <script
          type="text/javascript"
          src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"
        ></script>
        <script
          type="text/javascript"
          src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"
        ></script>
        <script src="js/slide.js" type="text/javascript"></script>
    </body>
</html> 