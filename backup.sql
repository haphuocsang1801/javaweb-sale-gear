USE [PROJECT_PRJ]
GO
/****** Object:  Table [dbo].[NSX]    Script Date: 11/8/2021 3:35:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NSX](
	[nsx_id] [nvarchar](50) NOT NULL,
	[nsx_name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_NSX] PRIMARY KEY CLUSTERED 
(
	[nsx_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[order_item]    Script Date: 11/8/2021 3:35:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[order_item](
	[quantity] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[price] [float] NOT NULL,
	[pro_id] [nvarchar](50) NOT NULL,
	[date_of_payment] [date] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[product_detail]    Script Date: 11/8/2021 3:35:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[product_detail](
	[pro_id] [nvarchar](50) NOT NULL,
	[pro_name] [nvarchar](max) NULL,
	[ptyle_id] [int] NULL,
	[price] [float] NULL,
	[description] [nvarchar](max) NULL,
	[nsx_id] [nvarchar](50) NULL,
	[image] [nvarchar](max) NULL,
	[quantity] [int] NULL,
	[date] [date] NULL,
 CONSTRAINT [PK_product_detail] PRIMARY KEY CLUSTERED 
(
	[pro_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[product_type]    Script Date: 11/8/2021 3:35:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[product_type](
	[ptyle_id] [int] NOT NULL,
	[ptyle_name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_product_type] PRIMARY KEY CLUSTERED 
(
	[ptyle_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sold_product]    Script Date: 11/8/2021 3:35:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sold_product](
	[pro_id] [nvarchar](50) NOT NULL,
	[sold_quantity] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user]    Script Date: 11/8/2021 3:35:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user](
	[user_id] [int] IDENTITY(1,1) NOT NULL,
	[user_name] [nvarchar](50) NOT NULL,
	[password] [nvarchar](50) NOT NULL,
	[full_name] [nvarchar](50) NOT NULL,
	[phone] [nvarchar](50) NOT NULL,
	[address] [nvarchar](max) NOT NULL,
	[email] [nvarchar](50) NOT NULL,
	[roleID] [nvarchar](2) NOT NULL,
 CONSTRAINT [PK_user] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[NSX] ([nsx_id], [nsx_name]) VALUES (N'SX01', N'Asus')
INSERT [dbo].[NSX] ([nsx_id], [nsx_name]) VALUES (N'SX02', N'Acer')
INSERT [dbo].[NSX] ([nsx_id], [nsx_name]) VALUES (N'SX03', N'Dell')
INSERT [dbo].[NSX] ([nsx_id], [nsx_name]) VALUES (N'SX04', N'MSI')
INSERT [dbo].[NSX] ([nsx_id], [nsx_name]) VALUES (N'SX05', N'5-Team Build')
INSERT [dbo].[NSX] ([nsx_id], [nsx_name]) VALUES (N'SX06', N'Intel')
INSERT [dbo].[NSX] ([nsx_id], [nsx_name]) VALUES (N'SX07', N'GIGABYTE')
INSERT [dbo].[NSX] ([nsx_id], [nsx_name]) VALUES (N'SX08', N'Kingston')
INSERT [dbo].[NSX] ([nsx_id], [nsx_name]) VALUES (N'SX09', N'AMD')
INSERT [dbo].[NSX] ([nsx_id], [nsx_name]) VALUES (N'SX10', N'G.SKILL')
INSERT [dbo].[NSX] ([nsx_id], [nsx_name]) VALUES (N'SX11', N'Corsair')
INSERT [dbo].[NSX] ([nsx_id], [nsx_name]) VALUES (N'SX12', N'AORUS')
INSERT [dbo].[NSX] ([nsx_id], [nsx_name]) VALUES (N'SX13', N'SAMSUNG')
INSERT [dbo].[NSX] ([nsx_id], [nsx_name]) VALUES (N'SX14', N'Seagate')
INSERT [dbo].[NSX] ([nsx_id], [nsx_name]) VALUES (N'SX15', N'XIGMATEK')
INSERT [dbo].[NSX] ([nsx_id], [nsx_name]) VALUES (N'SX16', N'INWIN')
GO
INSERT [dbo].[order_item] ([quantity], [user_id], [price], [pro_id], [date_of_payment]) VALUES (2, 2, 4390000, N'SP28      ', CAST(N'2021-11-04' AS Date))
INSERT [dbo].[order_item] ([quantity], [user_id], [price], [pro_id], [date_of_payment]) VALUES (2, 2, 299000, N'SP29', CAST(N'2021-11-04' AS Date))
INSERT [dbo].[order_item] ([quantity], [user_id], [price], [pro_id], [date_of_payment]) VALUES (2, 2, 4390000, N'SP28', CAST(N'2021-11-04' AS Date))
INSERT [dbo].[order_item] ([quantity], [user_id], [price], [pro_id], [date_of_payment]) VALUES (2, 1, 4390000, N'SP28', CAST(N'2021-11-04' AS Date))
INSERT [dbo].[order_item] ([quantity], [user_id], [price], [pro_id], [date_of_payment]) VALUES (1, 1, 7290000, N'SP38', CAST(N'2021-11-05' AS Date))
INSERT [dbo].[order_item] ([quantity], [user_id], [price], [pro_id], [date_of_payment]) VALUES (1, 2, 1790000, N'SP22', CAST(N'2021-11-05' AS Date))
INSERT [dbo].[order_item] ([quantity], [user_id], [price], [pro_id], [date_of_payment]) VALUES (1, 2, 20290000, N'SP01', CAST(N'2021-11-05' AS Date))
INSERT [dbo].[order_item] ([quantity], [user_id], [price], [pro_id], [date_of_payment]) VALUES (1, 2, 12490000, N'SP35', CAST(N'2021-11-05' AS Date))
INSERT [dbo].[order_item] ([quantity], [user_id], [price], [pro_id], [date_of_payment]) VALUES (1, 2, 5290000, N'SP30', CAST(N'2021-11-06' AS Date))
INSERT [dbo].[order_item] ([quantity], [user_id], [price], [pro_id], [date_of_payment]) VALUES (1, 1, 20290000, N'SP01', CAST(N'2021-11-06' AS Date))
INSERT [dbo].[order_item] ([quantity], [user_id], [price], [pro_id], [date_of_payment]) VALUES (2, 1, 28990000, N'SP32', CAST(N'2021-11-06' AS Date))
INSERT [dbo].[order_item] ([quantity], [user_id], [price], [pro_id], [date_of_payment]) VALUES (1, 4, 980000, N'SP46', CAST(N'2021-11-07' AS Date))
INSERT [dbo].[order_item] ([quantity], [user_id], [price], [pro_id], [date_of_payment]) VALUES (1, 2, 15490000, N'SP39', CAST(N'2021-11-07' AS Date))
INSERT [dbo].[order_item] ([quantity], [user_id], [price], [pro_id], [date_of_payment]) VALUES (2, 1, 299000, N'SP29', CAST(N'2021-11-04' AS Date))
INSERT [dbo].[order_item] ([quantity], [user_id], [price], [pro_id], [date_of_payment]) VALUES (3, 1, 28990000, N'SP32', CAST(N'2021-11-06' AS Date))
INSERT [dbo].[order_item] ([quantity], [user_id], [price], [pro_id], [date_of_payment]) VALUES (1, 2, 5990000, N'SP37', CAST(N'2021-11-07' AS Date))
INSERT [dbo].[order_item] ([quantity], [user_id], [price], [pro_id], [date_of_payment]) VALUES (1, 2, 28290000, N'SP14', CAST(N'2021-11-07' AS Date))
GO
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP01', N'Laptop Gaming Acer Aspire 7 A715 75G 56ZL', 2, 20290001, N'Acer Aspire 7 A715 75G 56ZL là một chiếc laptop thuộc phân khúc gaming, sở hữu thiết kế gọn gàng, thanh lịch tối giản mang phong cách văn phòng.', N'SX02', N'.\image\Laptop Gaming Acer Aspire 7 A715 75G 56ZL\img.png', 3, CAST(N'2021-10-26' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP02', N'Laptop ASUS TUF Gaming F15 FX506LH HN002T', 2, 20990000, N'Laptop cao cấp vượt trội với CPU Intel Core i5-10300H và GPU GeForce GTX™ 1650 mạnh mẽ, các tựa game hành động sẽ chạy nhanh, mượt mà và khai thác tối đa màn hình IPS tần số quét 144Hz. Mặc dù có khung máy di động và nhỏ hơn so với thế hệ tiền nhiệm, chiếc laptop này vẫn có dung lượng pin (48Wh/90Wh) cho thời lượng pin dài. Hệ thống tản nhiệt tự làm sạch hiệu quả kết hợp với độ bền đạt chuẩn quân đội của TUF giúp chiếc máy trở thành chiến binh bền bỉ đáng tin cậy cho các game thủ.', N'SX01', N'.\image\Laptop ASUS TUF Gaming F15 FX506LH HN002T\img.png', 2, CAST(N'2021-10-26' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP03', N'Laptop Gaming Asus ROG Strix G15 G513IH HN015T', 2, 22990000, N'Phong cách thể thao thể hiện qua ba màu sắc khác biệt giúp nâng tầm diện mạo và phong cách của bạn. Những phiên bản với màu đen nguyên bản Original Black, xám cực chất Eclipse Gray, và Electro Punk rực rỡ sẽ thể hiện phong cách của bạn. Chơi game tại bất kỳ đâu với khung máy có kích thước nhỏ hơn đến 7% và gọn nhẹ hơn những thế hệ tiền nhiệm. Những biểu tượng và đường cắt tinh tế tô điểm bên ngoài máy và thậm chí thêm phần thu hút ở phần đế máy, tạo điểm nhấn khác biệt từ mọi góc độ. Làm bừng sáng cho môi trường xung quanh bạn với hệ thống Aura Sync nổi bật từ logo kim loại của ROG, dọc bàn phím có đèn nền tới dải đèn chiếu sáng ở mặt đáy.

', N'SX01', N'.\image\Laptop Gaming Asus ROG Strix G15 G513IH HN015T\img.png', 3, CAST(N'2021-10-26' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP04', N'Laptop Gaming Asus TUF Dash F15 FX516PC HN002T', 2, 24990000, N'Laptop ASUS TUF Dash F15 FX516PC-HN002T có thiết kế táo bạo và mạnh mẽ sẽ giúp bạn đa nhiệm nhiều hơn trên một chiếc máy tính xách tay đủ nhẹ để mang đi khắp mọi nơi.', N'SX01', N'.\image\Laptop ASUS TUF Gaming F15 FX506LH HN002T\img.png', 2, CAST(N'2021-10-26' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP05', N'Laptop gaming Acer Nitro 5 AN515 45 R0B6', 2, 30990000, N'Acer Nitro 5 2021 AN515 45 R0B6 tích hợp những “vũ khí” mới nhất. Bao gồm CPU AMD Ryzen 7 5800H, VGA NVIDIA GeForce RTX 3060 cho hiệu năng xử lý mạnh mẽ.', N'SX02', N'.\image\Laptop gaming Acer Nitro 5 AN515 45 R0B6\img.png', 5, CAST(N'2021-10-26' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP06', N'Laptop Gaming Asus ROG Flow X13 GV301QC K6029T', 2, 79990000, N'Chúng ta có sức mạnh vượt trội từ chip xử lý AMD Ryzen™ 9 5980HS 8 nhân 16 luồng đi kèm 32GB Ram và card đồ hoạ mạnh mẽ trong máy RTX 3050 4GB GDDR6 cùng card đồ hoạ rời để khi sử dụng thì cắm vào là eGPU ROG XG Mobile: NVIDIA GeForce RTX 3080 (1810MHz at 150W). Tính di động phải đi kèm với hiệu năng rất tốt.', N'SX01', N'.\image\Laptop Gaming Asus ROG Flow X13 GV301QC K6029T\img.png', 1, CAST(N'2021-10-26' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP07', N'Laptop Dell Vostro 3400 70235020', 3, 15190000, N'Laptop Dell Vostro 3400 với bộ xử lý mới nhất của nhà Intel Core i3 Tiger Lake thế hệ 11 cho máy khả năng xử lí trơn tru mọi tác vụ, đáp ứng tốt nhu cầu đồ họa và tiết kiệm lượng. Bộ nhớ RAM 8 GB cho máy khả năng thao tác đa nhiệm một cách mượt mà và ổn định. 

Bên cạnh đó, trang bị ổ cứng SSD với dung lượng lên đến 256GB cho máy tốc độ truy xuất dữ liệu nhanh chóng.', N'SX03', N'.\image\Laptop Dell Vostro 3400 70235020\img.jpg', 5, CAST(N'2021-10-26' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP08', N'Laptop ASUS VivoBook A515EA BQ498T', 3, 17900000, N'Asus Vivobook 15 mang đến sự tinh tế, tao nhã nhưng cũng có chút năng động cho năng lượng làm việc hằng ngày của bạn. Với màu sắc nổi bật và viền Neon vàng động đáo tạo điểm nhấn tại phím enter, Vivobook 15 là thiết kế cá tính nhưng không kém phần tinh tế.', N'SX01', N'.\image\Laptop ASUS VivoBook A515EA BQ498T\img.jpg', 5, CAST(N'2021-10-26' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP09', N'Laptop Dell Inspiron 3511 P112F001BBL (Black)', 3, 19290000, N'Bộ xử lý Intel i5-1135G7 thực sự không làm người dùng thất vọng, bởi mọi tác vụ văn phòng đều được hoàn thành trơn tru, nhanh chóng. So với thế hệ trước thì máy hoạt động nhanh, mạnh hơn rất nhiều lần. Hỗ trợ CPU, RAM 4GB DDR4 cũng tăng tốc độ phản hồi của dell inspiron 3511 khiến việc sử dụng trở lên mượt mà hơn. Tuy nhiên, để sử dụng những phần mềm nặng, có lẽ người dùng cần nâng cấp thêm RAM để hỗ trợ, vì máy có 2 slot RAM.

Bộ nhớ SSD 512GB M.2 PCIe cũng giúp bạn có thể lưu trữ những tệp dữ liệu lớn nhanh chóng, đơn giản.

Nói về card VGA, nếu muốn sử dụng các phần mềm thiết kế hình ảnh, video như PS, AI, PR... thì hoàn toàn có thể hoạt động mượt mà nhờ Intel Iris Xe Graphics. Lưu ý là bạn không nên bật nhiều ứng dụng một lúc để đảm bảo tốc độ xử lý của máy.', N'SX03', N'.\image\Laptop Dell Inspiron 3511 P112F001BBL (Black)\img.jpg', 5, CAST(N'2021-10-26' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP10', N'Laptop Asus Zenbook Duo UX482EA KA081T', 3, 32490000, N'Mang thiết kế mỏng nhẹ, công nghệ màn hình phụ ScreenPad™ Plus và chứa hiệu năng mạnh mẽ nhờ vi xử lý Intel Core thế hệ 11, laptop Asus Zenbook Duo UX482EA KA081T mang đến cho bạn trải nghiệm làm việc trên laptop hiện đại nhất, chuyên nghiệp và đầy phong cách.', N'SX01', N'.\image\Laptop Asus Zenbook Duo UX482EA KA081T\img.jpg', 3, CAST(N'2021-10-26' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP11', N'Laptop MSI Summit E13 Flip Evo A11MT 211VN', 3, 37990000, N'Sáng tạo trực tiếp ngay trên màn hình cảm ứng của MSI, khi đang trình chiếu hay họp nhóm bạn có thể thao tác chỉnh sửa bằng tay nhanh chóng không cần phải di chuột hay gõ phím phức tạp. Đi kèm là bút cảm ứng MSI được trang bị công nghệ MPP2.0 cho thao tác đồ họa thêm phần chính xác và linh động hơn.kết hợp hoàn hảo với màn hình cảm ứng là bản lề xoay gập 360 độ cho bạn khả năng có thể sử dụng máy tính với nhiều tư thế khác nhau hay dùng như một chiếc máy tính bảng thực thụ cỡ lớn, dễ dàng chia sẻ ý tưởng với mọi người mà không cần xoay chuyển hướng rườm rà.', N'SX04', N'.\image\Laptop MSI Summit E13 Flip Evo A11MT 211VN\img.jpg', 3, CAST(N'2021-10-26' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP12', N'PC AXE M', 1, 8890000, NULL, N'SX05', N'.\image\AXE M\img.jpg', 3, CAST(N'2021-10-26' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP13', N'PC Yuumi M
', 1, 22190000, NULL, N'SX05', N'.\image\Yuumi M\img.jpg', 3, CAST(N'2021-10-26' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP14', N'PC Ghost S', 1, 28290000, NULL, N'SX05', N'.\image\Ghost S\img.jpg', 2, CAST(N'2021-10-26' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP15', N'Garen S', 1, 42290000, NULL, N'SX05', N'.\image\Garen S\img.jpg', 3, CAST(N'2021-10-26' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP16', N'Predator Z', 1, 77390000, NULL, N'SX05', N'.\image\Predator Z\img.jpg', 2, CAST(N'2021-10-26' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP17', N'Urus X', 1, 113990000, NULL, N'SX05', N'.\image\Urus X\img.jpg', 2, CAST(N'2021-10-26' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP18', N'G-Station K302', 4, 23990000, NULL, N'SX05', N'.\image\G-Station K302\img.png', 5, CAST(N'2021-10-26' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP19', N'G-Creator C704', 4, 115590000, NULL, N'SX05', N'.\image\G-Creator C704\img.png', 3, CAST(N'2021-10-26' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP20', N'Homework I7', 4, 17990000, NULL, N'SX05', N'.\image\Homework I7\img.jpg', 5, CAST(N'2021-10-26' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP21', N'Intel® NUC NUC10i7FNH', 4, 13990000, NULL, N'SX06', N'.\image\Intel NUC NUC10i7FNH\img.jpg', 4, CAST(N'2021-10-26' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP22', N'ASUS PRIME H410M-F', 5, 1790000, N'Asus PRIME H410M-F nằm trong dòng sản phẩm ASUS Prime, được thiết kế chuyên nghiệp để phát huy hết tiềm năng của bộ vi xử lý Intel ® Core ™ thế hệ thứ 10 . Tự hào với thiết kế năng lượng mạnh mẽ, các giải pháp làm mát toàn diện và các tùy chọn điều chỉnh thông minh, bo mạch chủ dòng Prime H410 cung cấp cho người dùng hàng ngày và các nhà xây dựng PC DIY một loạt các tùy chọn điều chỉnh hiệu suất thông qua phần mềm và phần cứng đi kèm.', N'SX01', N'.\image\Mainboard\ASUS PRIME H410M-F\img.png', 8, CAST(N'2021-10-15' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP23', N'GIGABYTE B460M AORUS PRO (rev. 1.0)', 5, 2690000, N'Các bo mạch chủ của GIGABYTE 400 sử dụng thiết kế MOSFET kỹ thuật số 4 + 2 Hybrid + RDS (bật) thấp hơn để hỗ trợ CPU Intel ®  Core ™ mới nhất bằng cách cung cấp độ chính xác đáng kinh ngạc trong việc cung cấp năng lượng cho các thành phần nhạy cảm với năng lượng và nhạy cảm nhất với bo mạch chủ. như cung cấp hiệu suất hệ thống nâng cao và khả năng mở rộng phần cứng cuối cùng.', N'SX07', N'.\image\Mainboard\GIGABYTE B460M AORUS PRO (rev. 1.0)\img.png', 10, CAST(N'2021-10-12' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP24', N'MSI MPG Z590 GAMING CARBON WIFI', 5, 8190000, N'Mainboard MSI MPG Z590 GAMING CARBON WIFI là dòng Mainboard đỉnh cao mà các game thủ nên sử dụng. Giao diện BIOS thân thiện và dễ dàng sử dụng, hỗ trợ người dùng tùy chỉnh hệ thống một cách thuận tiện và dễ dàng hơn. Z590 là thế hệ chipset mới nhất của Intel dành cho các CPU Core i thế hệ 10 như Core i9-10900K, i7-10700K, v.v… Với việc nâng cấp lên 10 nhân / 20 luồng ở mã cao cấp nhất ( 10900K ), yêu cầu một bo mạch chủ đủ tốt để có thể hoạt động ổn định. MSI đã cho ra mắt hàng loạt các bo mạch chủ thế hệ mới với những công nghệ định cao đủ để đáp ứng tốt nhất từ sử dụng cơ bản cho tới ép xung mạnh mẽ.', N'SX04', N'.\image\Mainboard\MSI MPG Z590 GAMING CARBON WIFI\img.png', 8, CAST(N'2021-10-27' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP25', N'ASUS Z590 ROG MAXIMUS XIII EXTREME GLACIAL', 5, 35990000, N'Đứa con băng giá từ nhà ASUS, ASUS Z590 ROG MAXIMUS XIII EXTREME GLACIAL được hợp tác với thương hiệu sản xuất tản nhiệt nổi tiếng EK đã cùng nhau tạo nên một sản phẩm mang trên mình thiết kế mạnh mẽ đến từ góc cạnh cho bo mạch chủ cùng tông màu trắng làm bật lên cái tên GLACIAL của mình. Đặt vào tầm mắt chúng ta có là block nước tản nhiệt vô cùng hiện đại EK đã cung cấp trên ASUS Z590 ROG MAXIMUS XIII EXTREME GLACIAL. Tất cả cùng tạo nên một trái tim băng giá vô cùng mạnh mẽ.', N'SX01', N'.\image\Mainboard\ASUS Z590 ROG MAXIMUS XIII EXTREME GLACIAL\img.jpg', 6, CAST(N'2021-10-28' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP26', N'GIGABYTE Z590 AORUS XTREME WATERFORCE (rev. 1.0)', 5, 44990000, N'Mainboard Gigabyte Z590 AORUS XTREME WATERFORCE là một trong những dòng bo mạch chủ vơus thiết kế hiện đại, chức năng tuyệt vời, thiết kế tản nhiệt ấn tượng, kết nối thế hệ tiếp theo, hệ thống âm thanh cấp độ Hi-Fi và tính thẩm mỹ AORUS. Để phát huy hết tiềm năng của CPU Intel thế hệ thứ 11 mới, bo mạch chủ yêu cầu sức mạnh CPU, bộ nhớ và thiết kế IO tốt nhất. Với các thành phần chất lượng tốt nhất và khả năng thiết kế R&D của GIGABYTE, Z590 AORUS XTREME WATERFORCE là một con quái vật thực sự trong số các bo mạch chủ.', N'SX07', N'.\image\Mainboard\GIGABYTE Z590 AORUS XTREME WATERFORCE (rev. 1.0)\img.png', 5, CAST(N'2021-10-27' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP27', N'Intel Core i3 10100F / 6MB / 4.3GHZ / 4 nhân 8 luồng / LGA 1200', 6, 2490000, N'CPU Intel Core i3-10100F / 6MB / 4.3GHZ / 4 nhân 8 luồng, chiếc CPU 10th Gen mới nhất từ đội xanh Intel. Sở hữ hiệu năng mạnh mẽ cùng mức giá hợp lý từ việc không trang bị GPU tích hợp, i3-10100F trở thành kẻ thách thức với những chiếc CPU giá rẻ cùng phân khúc hiện nay, đặc biệt là CPU AMD Ryzen 3 3100.', N'SX06', N'.\image\CPU\i3\img.jpg', 5, CAST(N'2021-10-29' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP28', N'Intel Core i5 10400F / 12MB / 4.3GHz / 6 Nhân 12 Luồng / LGA 1200', 6, 4390000, N'CPU Intel Core i5-10400F đã khiến thị trường dòng chip tầm trung "nóng" hơn rất nhiều. Với 6 nhân 12 luồng, xung nhịp 2.9GHz và turbo boost lên 4.3GHz, quả là là sự chọn tuyệt vời cho việc chơi game cho tới stream game, vượt xa người tiền nhiệm i5-9400F khi không thể hoàn thành tốt việc stream game.', N'SX06', N'.\image\CPU\Intel Core i5 10400F  12MB  4.3GHz  6 nhan 12 luong LGA 1200\img.jpg', 0, CAST(N'2021-11-02' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP29', N'AMD Ryzen 3 3200G / 6MB / 4.0GHz / 4 nhân 4 luồng / AM4', 6, 299000, N'Nếu bạn là một game thủ hay một người dùng văn phòng cần giải trí nhẹ nhàng với các game esport như liên minh huyền thoại, CS:Go với bạn bè mà không muốn chi ra một hầu bao quá lớn thì con chip AMD Ryzen 3 3200G /6MB /3.6GHz /4 nhân 4 luồng này chính là những gì bạn cần để vui vẻ một chút với anh em ngày cuối tuần mà vẫn đảm bảo hoàn thành tốt mọi công việc của mình.', N'SX09', N'.\image\CPU\AMD Ryzen 3 3200G  6MB  4.0GHz  4 nhan 4 luong  AM4\img.jpg', 1, CAST(N'2021-11-02' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP30', N'AMD Ryzen 5 3600  32MB  4.2GHz  6 nhân 12 luồng  AM4', 6, 5290000, N'Bộ CPU AMD Ryzen 5 3600 của AMD đóng vai trò là đòn đánh trực diện vào vị trí cao cấp của Intel là Core i5-9600K. Sở hữu thông số ấn tượng, 6 nhân, 12 luồng xử lý, bộ nhớ đệm lên tới 32MB, và điều đặc biệt nhất là tốc độ xử lý tối đa khi hoạt động ở chế độ đơn nhân lên tới 4.2GHz, điều mà trước đây chưa bộ CPU Ryzen nào làm được. ', N'SX09', N'.\image\CPU\AMD Ryzen 5 3600  32MB  4.2GHz  6 nhan 12 luong AM4\img.jpg', 3, CAST(N'2021-11-02' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP31', N'AMD Ryzen Threadripper PRO 3995WX / Socket sWRX80 / 256MB / 4.2Ghz / 64 nhân 128 luồng', 6, 141000000, N'AMD Ryzen Threadripper PRO 3995WX là dòng CPU dành cho máy trạm mạnh mẽ nhất hiện nay. Dòng AMD Ryzen Threadripper Pro có mọi thứ cơ bản mà dòng Threadripper trước đó đã có và nâng tầm tất cả lên một đẳng cấp khác bằng cách mở khóa hệ số nhân, hỗ trợ ép xung cho máy trạm, nâng cấp dung lượng bộ nhớ cao hơn và khả năng kết nối thiết bị ngoại vi.', N'SX09', N'.\image\CPU\AMD Ryzen Threadripper PRO 3995WX  Socket sWRX80  256MB  4.2Ghz  64 nhan 128 luong\img.png', 3, CAST(N'2021-11-02' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP32', N'Intel Core i9 10980XE  24.75MB  4.6GHz  18 nhân 36 luồng  LGA 2066', 6, 28990000, NULL, N'SX06', N'.\image\CPU\Intel Core i9 10980XE  24.75MB  4.6GHz  18 nhan 36 luong LGA 2066\img.png', 0, CAST(N'2021-11-02' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP33', N'(8GB DDR4 1x8G 2666) RAM Kingston HyperX Fury Black', 7, 1090000, N'HyperX FURY DDR4 là lựa chọn hoàn hảo cho nhà xây dựng hệ thống hoặc người muốn tìm một giải pháp nâng cấp để tăng tốc cho hệ thống chậm chạp của mình.', N'SX08', N'.\image\RAM\(8GB DDR4 1x8G 2666) RAM Kingston HyperX Fury Black\img.jpg', 10, CAST(N'2021-11-02' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP34', N'(8GB DDR4 1x8G 3000) RAM G.SKILL Trident Z RGB CL16-18-18-38', 7, 1450000, N'Trident Z RGB vẫn giữ nguyên yếu tố thiết kế mang tính biểu tượng của dòng sản phẩm Trident Z truyền thống - có các bộ tản nhiệt bằng nhôm hoàn thiện dạng sợi tóc sang trọng và thiết kế vây tích cực để tản nhiệt hiệu quả cao. Phần trên cùng của bộ tản nhiệt đã được thiết kế độc quyền để gắn bộ khuếch tán ánh sáng rộng hơn cho hiệu ứng ánh sáng lộng lẫy hơn. Không cần tìm đâu xa cho một bộ nhớ ram kết hợp hiệu năng và vẻ đẹp để xây dựng một chiếc PC hiện đại, phong cách.', N'SX10', N'.\image\RAM\(8GB DDR4 1x8G 3000) RAM G.SKILL Trident Z RGB CL16-18-18-38\img.jpg', 10, CAST(N'2021-11-02' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP35', N'(64G DDR4 2x32G 3200) Corsair Vengeance RGB PRO CL16-18-18-36', 7, 12490000, N'Corsair Vengeance RGB Pro Series là một trong những kit ram DDR4 cho hiệu năng khá tốt không những vậy Corsair Vengeance RGB Pro được trang bị thêm dãi LED RBG cực kỳ đẹp mắt.', N'SX11', N'.\image\RAM\(64G DDR4 2x32G 3200) Corsair Vengeance RGB PRO CL16-18-18-36\img.jpg', 3, CAST(N'2021-11-02' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP36', N'(32G DDR4 2x16G 3600) G.SKILL Trident Z Neo DDR4 CL16-16-16-36', 7, 7380000, N'Được thiết kế và tối ưu hóa để tương thích hoàn toàn trên bộ xử lý AMD Ryzen 3000 mới nhất, Trident Z Neo mang đến hiệu năng bộ nhớ tuyệt vời và ánh sáng RGB sống động', N'SX10', N'.\image\RAM\(32G DDR4 2x16G 3600) G.SKILL Trident Z Neo DDR4 CL16-16-16-36\img.jpg', 5, CAST(N'2021-11-02' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP37', N'ASUS Cerberus GeForce GTX 1050 Ti OC 4GB', 8, 5990000, NULL, N'SX01', N'.\image\VGA\ASUS Cerberus GeForce GTX 1050 Ti OC 4GB\img.png', 4, CAST(N'2021-11-02' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP38', N'MSI GeForce GTX 1650 D6 GAMING X', 8, 7290000, N'MSI GeForce GTX 1650 D6 GAMING X được tạo nên từ kiến trúc Turing đem lại khả năng xử lý hình ảnh tăng 1.4 lần, giúp trải nghiệm chơi game vượt bậc và tuyệt vời hơn. Hiệu năng cũng được nâng cấp mạnh mẽ: gấp đôi với GTX 950 và hơn 70% so với GTX 1050. Cùng ứng dụng GeForce Experience nâng cao hiệu quả sử dụng người dùng với các chức năng chụp màn hình, livestream và tối ưu hóa game.', N'SX04', N'.\image\VGA\MSI GeForce GTX 1650 D6 GAMING X\img.jpg', 4, CAST(N'2021-11-02' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP39', N'ASUS Dual GeForce RTX 2060 OC EVO 6GB GDDR6', 8, 15490000, N'Là phiên bản nâng cấp từ phiên bản RTX 2060 OC Edition trước đó, ASUS Dual GeForce RTX 2060 OC edition EVO vẫn giữa được vẻ ngoài gaming trước đó. Khoác lên màu đen mạnh mẽ cùng các góc cạnh được hoàn thiện một cách bắt mắt, ASUS Dual GeForce RTX 2060 OC edition EVO sẽ làm nổi bật chiếc case nói riêng và góc gaming nói chung.

', N'SX01', N'.\image\VGA\ASUS Dual GeForce RTX 2060 OC EVO 6GB GDDR6\img.png', 4, CAST(N'2021-11-02' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP40', N'GIGABYTE Radeon RX 6600 EAGLE 8G', 8, 14990000, NULL, N'SX07', N'.\image\VGA\GIGABYTE Radeon RX 6600 EAGLE 8G\img.png', 5, CAST(N'2021-11-02' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP41', N'GIGABYTE AORUS GeForce RTX 3090 XTREME WATERFORCE 24G', 8, 67990000, N'Card màn hình Gigabyte RTX 3090 XTREME WATERFORCE-24G là phiên bản tích hợp sẵn tản nhiệt nước dạng All In One của Gigabyte cho hiệu quả làm mát vượt trội so với tản nhiệt khí thông thường. ', N'SX07', N'.\image\VGA\GIGABYTE AORUS GeForce RTX 3090 XTREME WATERFORCE 24G\img.png', 5, CAST(N'2021-11-02' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP42', N'SSD AORUS AIC NVMe SSD 1TB RGB', 9, 5550000, NULL, N'SX12', N'.\image\SSD\SSD AORUS AIC NVMe SSD 1TB RGB\img.jpg', 8, CAST(N'2021-11-02' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP43', N'SSD Samsung 970 Evo Plus 2TB M.2 NVMe', 9, 10490000, NULL, N'SX13', N'.\image\SSD\SSD Samsung 970 Evo Plus 2TB M.2 NVMe\img.jpg', 8, CAST(N'2021-11-02' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP44', N'Ổ Cứng Di Động SSD Seagate Firecuda Gaming 2Tb USB-C (STJP2000400)', 9, 14990000, NULL, N'SX14', N'.\image\SSD\SSD Seagate Firecuda Gaming 2Tb USB-C (STJP2000400)\img.png', 8, CAST(N'2021-11-02' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP45', N'SSD Samsung 870 QVO 8TB 2.5 Inch SATA III', 9, 20490000, NULL, N'SX13', N'.\image\SSD\SSD Samsung 870 QVO 8TB 2.5 Inch SATA III\img.jpg', 8, CAST(N'2021-11-02' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP46', N'HDD Seagate Barracuda 1TB 7200rpm', 10, 980000, N'Ổ cứng seagate đa năng cho mọi nhu cầu máy tính của bạn mang đến cho bạn sự xuất sắc hàng đầu trong ngành máy tính cá nhân.
Trong hơn 20 năm, gia đình BarraCuda đã cung cấp lưu trữ siêu đáng tin cậy cho công nghiệp ổ cứng.', N'SX14', N'.\image\HDD\HDD Seagate Barracuda 1TB 7200rpm\img.jpg', 8, CAST(N'2021-11-02' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP47', N'HDD Seagate Ironwolf PRO 14TB 7200rpm', 10, 14490000, N'Với chất lượng vượt trội và hiệu năng cao. HDD Seagate  là một sự lựa chọn tối ưu khi bạn muốn nâng cấp ổ cứng.Tối ưu hóa cho NAS với AgileArray™ và khả năng phục hồi lỗi giúp cải thiện chất lượng của ổ cứng hoạt động trong dãy RAID. HDD Seagate IronWolf Pro với gói phục hồi và cứu hộ sẽ cho bạn sự an tâm tuyệt đối khi sử dụng.', N'SX14', N'.\image\HDD\HDD Seagate Ironwolf PRO 14TB 7200rpm\img.jpg', 10, CAST(N'2021-11-02' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP48', N'Case XIGMATEK XA-20 (ATX)', 11, 330000, N'Thiết kế chiếc case cứng cáp và bền bỉ, hỗ trợ đa dạng mainboard từ ITX cho đến ATX. Không gian case rộng rãi thoáng khí giúp tản nhiệt rất tốt.', N'SX15', N'.\image\Case\Case XIGMATEK XA-20 (ATX)\img.png', 15, CAST(N'2021-11-02' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP49', N'Case XIGMATEK VENOM', 11, 890000, N'The Venom là loạt khung máy chơi game sáng tạo mới nhất của XIGMATEK. Các tính năng bao gồm: các tấm kính cường lực ở bên trái và phía trước để hiển thị hoàn hảo mọi thành phần của bản dựng, giải phóng mặt bằng cho bộ làm mát CPU 160 mm, giải phóng mặt bằng GPU 380 mm và có thể dập tắt bộ tản nhiệt làm mát bằng chất lỏng (phía trước lên đến 240 mm). Tấm vải liệm PSU dành cho quản lý cáp với màn hình gọn gàng hơn.

', N'SX15', N'.\image\Case\Case XIGMATEK VENOM\img.jpg', 10, CAST(N'2021-11-02' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP50', N'Case INWIN D-Frame 2.0', 11, 28000000, NULL, N'SX16', N'.\image\Case\Case INWIN D-Frame 2.0\img.jpg', 5, CAST(N'2021-11-02' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP51', N'Case INWIN H-Frame 2.0', 11, 33990000, NULL, N'SX16', N'.\image\Case\Case INWIN H-Frame 2.0\img.jpg', 5, CAST(N'2021-11-02' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP52', N'Case INWIN WINBOT', 11, 89990000, NULL, N'SX16', N'.\image\Case\Case INWIN WINBOT\img.jpg', 3, CAST(N'2021-11-02' AS Date))
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP53', N'Test', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[product_detail] ([pro_id], [pro_name], [ptyle_id], [price], [description], [nsx_id], [image], [quantity], [date]) VALUES (N'SP54', N'Laptop Gaming ASUS ROG Zephyrus G14 GA401QM K2041T', 2, 48000000, N'MÃ n hÃ¬nh hiá»n thá» tuyá»t Äáº¹p â WQHD 120Hz Äá» phÃ¢n giáº£i cao vÃ  gam mÃ u DCI-P3 chuyÃªn nghiá»p lÃ m má»i cao giÃºp báº¡n Äáº¯m chÃ¬m vá»i mÃ u sáº¯c ÄÃ£ ÄÆ°á»£c Pantone Â® xÃ¡c thá»±c sá»ng Äá»ng nhÆ° tháº­t. Táº¥m ná»n nÃ y cÅ©ng há» trá»£ Adaptive-Sync, Äá»ng bá» hÃ³a mÃ n hÃ¬nh vÃ  GPU Äá» loáº¡i bá» hiá»n tÆ°á»£ng xÃ© hÃ¬nh vÃ  giÃºp quÃ¡ trÃ¬nh chÆ¡i game trá» nÃªn mÆ°á»£t mÃ . Bá»n loa phÃ¡t ra Ã¢m thanh Dolby Atmos tÆ°Æ¡i tá»t, trong khi tÃ­nh nÄng Chá»ng á»n AI hai chiá»u Äá»c quyá»n giÃºp cuá»c trÃ² chuyá»n báº±ng giá»ng nÃ³i trá» nÃªn rÃµ rÃ ng nhÆ° pha lÃª.

', N'SX01', N'.\image\k2041t_e048eff17ee24a0a8c2f173541395c32.png', 10, CAST(N'2021-11-07' AS Date))
GO
INSERT [dbo].[product_type] ([ptyle_id], [ptyle_name]) VALUES (1, N'PC Gaming')
INSERT [dbo].[product_type] ([ptyle_id], [ptyle_name]) VALUES (2, N'Laptop Gaming')
INSERT [dbo].[product_type] ([ptyle_id], [ptyle_name]) VALUES (3, N'Laptop Working & Studying')
INSERT [dbo].[product_type] ([ptyle_id], [ptyle_name]) VALUES (4, N'PC WorkStation')
INSERT [dbo].[product_type] ([ptyle_id], [ptyle_name]) VALUES (5, N'Mainboard')
INSERT [dbo].[product_type] ([ptyle_id], [ptyle_name]) VALUES (6, N'CPU')
INSERT [dbo].[product_type] ([ptyle_id], [ptyle_name]) VALUES (7, N'RAM')
INSERT [dbo].[product_type] ([ptyle_id], [ptyle_name]) VALUES (8, N'VGA')
INSERT [dbo].[product_type] ([ptyle_id], [ptyle_name]) VALUES (9, N'SSD')
INSERT [dbo].[product_type] ([ptyle_id], [ptyle_name]) VALUES (10, N'HDD')
INSERT [dbo].[product_type] ([ptyle_id], [ptyle_name]) VALUES (11, N'Case')
GO
INSERT [dbo].[sold_product] ([pro_id], [sold_quantity]) VALUES (N'SP29', 3)
INSERT [dbo].[sold_product] ([pro_id], [sold_quantity]) VALUES (N'SP28', 10)
INSERT [dbo].[sold_product] ([pro_id], [sold_quantity]) VALUES (N'SP38', 1)
INSERT [dbo].[sold_product] ([pro_id], [sold_quantity]) VALUES (N'SP22', 2)
INSERT [dbo].[sold_product] ([pro_id], [sold_quantity]) VALUES (N'SP01', 3)
INSERT [dbo].[sold_product] ([pro_id], [sold_quantity]) VALUES (N'SP35', 2)
INSERT [dbo].[sold_product] ([pro_id], [sold_quantity]) VALUES (N'SP30', 2)
INSERT [dbo].[sold_product] ([pro_id], [sold_quantity]) VALUES (N'SP32', 3)
INSERT [dbo].[sold_product] ([pro_id], [sold_quantity]) VALUES (N'SP37', 1)
INSERT [dbo].[sold_product] ([pro_id], [sold_quantity]) VALUES (N'SP46', 2)
INSERT [dbo].[sold_product] ([pro_id], [sold_quantity]) VALUES (N'SP39', 1)
INSERT [dbo].[sold_product] ([pro_id], [sold_quantity]) VALUES (N'SP14', 1)
GO
SET IDENTITY_INSERT [dbo].[user] ON 

INSERT [dbo].[user] ([user_id], [user_name], [password], [full_name], [phone], [address], [email], [roleID]) VALUES (1, N'admin', N'123456', N'sangha', N'0795911712', N'cantho', N'sang@gmail.com', N'AD')
INSERT [dbo].[user] ([user_id], [user_name], [password], [full_name], [phone], [address], [email], [roleID]) VALUES (2, N'customers', N'123456', N'sang', N'2102010120', N'cantho', N'sang1@gmail.com', N'US')
INSERT [dbo].[user] ([user_id], [user_name], [password], [full_name], [phone], [address], [email], [roleID]) VALUES (3, N'trungthanh', N'123456', N'thanh', N'1212312312', N'dqwdqdqwd', N'thanh@gmail.com', N'US')
INSERT [dbo].[user] ([user_id], [user_name], [password], [full_name], [phone], [address], [email], [roleID]) VALUES (4, N'sangha1', N'12345', N'sangha', N'0795911712', N'viet nam', N'sa@gmail.com', N'US')
SET IDENTITY_INSERT [dbo].[user] OFF
GO
ALTER TABLE [dbo].[order_item] ADD  CONSTRAINT [CONSTRAINT_NAME]  DEFAULT (getdate()) FOR [date_of_payment]
GO
ALTER TABLE [dbo].[order_item]  WITH CHECK ADD  CONSTRAINT [FK_order_item_product_detail] FOREIGN KEY([pro_id])
REFERENCES [dbo].[product_detail] ([pro_id])
GO
ALTER TABLE [dbo].[order_item] CHECK CONSTRAINT [FK_order_item_product_detail]
GO
ALTER TABLE [dbo].[order_item]  WITH CHECK ADD  CONSTRAINT [FK_order_item_user] FOREIGN KEY([user_id])
REFERENCES [dbo].[user] ([user_id])
GO
ALTER TABLE [dbo].[order_item] CHECK CONSTRAINT [FK_order_item_user]
GO
ALTER TABLE [dbo].[product_detail]  WITH CHECK ADD  CONSTRAINT [FK_product_detail_product_type] FOREIGN KEY([ptyle_id])
REFERENCES [dbo].[product_type] ([ptyle_id])
GO
ALTER TABLE [dbo].[product_detail] CHECK CONSTRAINT [FK_product_detail_product_type]
GO
ALTER TABLE [dbo].[product_detail]  WITH CHECK ADD  CONSTRAINT [up_nsx] FOREIGN KEY([nsx_id])
REFERENCES [dbo].[NSX] ([nsx_id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[product_detail] CHECK CONSTRAINT [up_nsx]
GO
ALTER TABLE [dbo].[sold_product]  WITH CHECK ADD  CONSTRAINT [FK_sold_product_product_detail] FOREIGN KEY([pro_id])
REFERENCES [dbo].[product_detail] ([pro_id])
GO
ALTER TABLE [dbo].[sold_product] CHECK CONSTRAINT [FK_sold_product_product_detail]
GO
